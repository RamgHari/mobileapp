import * as React from 'react';
import {Button, View,Text ,StyleSheet ,Image,TouchableOpacity} from 'react-native';
import {Avatar} from 'react-native-paper';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import DetailScreen from '../../tabScreens/Detail';
import HomeScreen from '../../tabScreens/home';
import CategoryScreen from '../../tabScreens/category';
import ProfileScreen from '../../tabScreens/Profile';
import ExploreScreen from '../../tabScreens/Explore';
import { MaterialCommunityIcons, FontAwesome5 } from "@expo/vector-icons";
import CoursesScreen from '../../stackScreens/course';
import CourseView from '../../stackScreens/view';
import CoursePlay from '../../stackScreens/coursePlay';
import Chapters from '../../../components/chapters';
import styles from './style.js';
import { colors } from "../../../assets/color/style";
import EnrollScreen from '../../stackScreens/enrollCourses';
import CourseChapters from '../../../components/coursechapter';
import CourseLessons from '../../../components/courseLessons';
import UserProfile from '../../stackScreens/userProfile';
import AddPostScreen from '../../addPost';
import CommentView from '../../stackScreens/comment';
import SinglePost from '../../stackScreens/singlePost';
import { useSelector} from 'react-redux';

const Tab = createMaterialBottomTabNavigator();
const MainTabScreen=(props)=>{
 
    return(
        <Tab.Navigator
      initialRouteName="Home"
      activeColor='#009387'
      barStyle={{ backgroundColor: colors.headerColor }}>
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        props="hhh"
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      {/* <Tab.Screen
        name="Courses"
        component={CategoryStackScreen}
        options={{
          tabBarLabel: 'Courses',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="apps" color={color} size={26} />
          ),
        }}
      /> */}
      {/* <Tab.Screen
        name="Details"
        component={ExploreScreen}
        options={{
          tabBarLabel: 'Category',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="play-circle" color={color} size={26} />
          ),
        }}
      /> */}
         <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
      
      {/* <Tab.Screen
        name="Explore"
        component={ExploreScreen}
        options={{
          tabBarLabel: 'Explore',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="sticker" color={color} size={26} />
          ),
        }}
      /> */}
    </Tab.Navigator>
    )
}

const HomeStack = createStackNavigator();
const AddPostStack = createStackNavigator();
const SinglePostViewStack=createStackNavigator();
const CommentViewStack=createStackNavigator();
const CourseStack = createStackNavigator();
const EnrollCourseStack = createStackNavigator();
const CourseViewStack = createStackNavigator();
const CoursePlayStack = createStackNavigator();
const CourseChapterViewStack=createStackNavigator();
const CourseLessonViewStack=createStackNavigator();
const ChapterViewStack= createStackNavigator();
const DetailStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const UserProfileStack = createStackNavigator();
const CategoryStack=createStackNavigator();



const HomeStackScreen=({ navigation })=> {
  
  //const [users,SetUsers]=React.useState({})
  const users=useSelector(state => state.userReducer.user)
  
    return (
     <HomeStack.Navigator screenOptions={{
       headerStyle:{
        elevation: 0
       },
       headerTintColor:'#009387',
       headerTitleStyle: { 
        textAlign:"center", 
        flex:1 ,
        fontFamily:"Bold"
    }
     }}>
    <HomeStack.Screen name="Home"  component={HomeScreen} 
          options={{
            title:'Feed',
            headerLeft:()=>(
              <TouchableOpacity onPress={()=>{navigation.openDrawer()}}>
              <View style={{
                paddingHorizontal:10,
                paddingVertical:12,
                borderRadius:10,
                marginLeft: 20,
                backgroundColor:"#009387"
            }}>
              <MaterialCommunityIcons name="menu" style={styles.actionButtonIcon}/>
              
            </View>
            </TouchableOpacity>
            ),
            headerRight: () => (
              <View style={{
                
                borderRadius:10,
                marginRight: 20,
                
            }}>
              <TouchableOpacity  onPress={()=>{navigation.navigate('Profile')}}>
                <Avatar.Image 
                source={{uri:users.userProfileImage}}
                size={50}
                />
                </TouchableOpacity>
                </View>
            ),
          }} />
          <AddPostStack.Screen name="AddPost" component={AddPostScreen}  />
          <CourseStack.Screen name="Courses" component={CoursesScreen}  />
          <CourseViewStack.Screen  options={{headerShown:false}} name="CourseView" component={CourseView}/>
          <ChapterViewStack.Screen  name="ChapterView" component={Chapters}/>
          <CourseChapterViewStack.Screen  name="CourseChapterView" component={CourseChapters}/>
          <CourseLessonViewStack.Screen  name="CourseLessonView" component={CourseLessons}/>
          <CoursePlayStack.Screen options={{headerShown:false}} name="CoursePlay" component={CoursePlay}/>
          <UserProfileStack.Screen options={{headerShown:false}} name="UserProfile" component={UserProfile}/>
          <CommentViewStack.Screen name="CommentView" component={CommentView }/>
          <SinglePostViewStack.Screen options={{headerShown:false}} name="SinglePost" component={SinglePost}/>
    </HomeStack.Navigator>
    );
  }


    const CategoryStackScreen=({ navigation }) =>{
      return (
       <CategoryStack.Navigator screenOptions={{
        headerTitleStyle: { 
         textAlign:"center", 
         flex:1 
     }
      }}>
      <CategoryStack.Screen name="Category" component={CategoryScreen}  
             options={{
              title:'',
              headerShown:false,
              headerLeft:()=>(
                <Icon.Button
                name="ios-menu"
                size={25}  onPress={()=>{navigation.openDrawer()}}></Icon.Button>
              )
            }} />
        <CourseStack.Screen  options={{headerShown:false}} name="Courses" component={CoursesScreen} />
        <EnrollCourseStack.Screen  options={{headerShown:false}} name="EnrollCourse" component={EnrollScreen} />
        <CourseViewStack.Screen  options={{headerShown:false}} name="CourseView" component={CourseView}/>
        <CoursePlayStack.Screen options={{headerShown:false}} name="CoursePlay" component={CoursePlay}/>
      </CategoryStack.Navigator>
      );
      }
      const DetailStackScreen=({ navigation }) =>{
        return (
         <DetailStack.Navigator screenOptions={{
          headerStyle:{
           backgroundColor:'#009387'
          },
          headerTintColor:'#fff',
          headerTitleStyle: { 
           textAlign:"center", 
           flex:1 
       }
        }}>
        <DetailStack.Screen name="Details" component={DetailScreen} 
               options={{
                title:'Detail Page',
                headerShown:false,
                headerLeft:()=>(
                  <Icon.Button
                  name="ios-menu"
                  size={25}  onPress={()=>{navigation.openDrawer()}}></Icon.Button>
                )
              }} />
        </DetailStack.Navigator>
        );
        }

    const ProfileStackScreen=({ navigation }) =>{
      return (
       <ProfileStack.Navigator screenOptions={{
        headerStyle:{
         backgroundColor:'#009387'
        },
        headerTintColor:'#fff',
        headerTitleStyle: { 
         textAlign:"center", 
         flex:1 
     }
      }}>
      <ProfileStack.Screen name="Profile" component={ProfileScreen} 
             options={{
              title:'Profile Page',
               headerShown:false,
              headerLeft:()=>(
                <View style={{
                  paddingHorizontal:10,
                  paddingVertical:12,
                  borderRadius:10,
                  marginLeft: 20,
                  backgroundColor:"#694fad"
              }}>
                <TouchableOpacity onPress={()=>{navigation.openDrawer()}}>
                  <Image
                   source={require('../../../assets/images/hum.png')}
                   style={{height:15,width:20}}
                  />
                  </TouchableOpacity>
              </View>
              ),
            }} />
         <CourseViewStack.Screen  options={{headerShown:false}} name="CourseView" component={CourseView}/>
        <CoursePlayStack.Screen options={{headerShown:false}} name="CoursePlay" component={CoursePlay}/>
      </ProfileStack.Navigator>
      );
      
    }

export default MainTabScreen;

