import React, { useEffect } from 'react';
import {View,Text,Image,TouchableOpacity,StyleSheet} from 'react-native';
import ProgressCircle from 'react-native-progress-circle';
import { colors } from "../../assets/color/style";
import { useNavigation } from '@react-navigation/native';
import { Ionicons, Feather } from "@expo/vector-icons";

 const CourseLessons=(props)=>{
    const {name,id,image,description}=props.lessonDetails
    const index=props.index
    const navigation = useNavigation();
    return(
                <View style={{
                    marginVertical: 10,
                    flexDirection: "row",
                  }}
                >
                  <TouchableOpacity
                    onPress={props.onPress}
                  >
                      {image? <Image
                     source={{uri:`https://www.allskills.in/uploads/course/lesson/${image}`}}

                      style={{
                        width: 120,
                        height: 80,
                        borderWidth: 1,
                        borderColor: colors.textGray,
                      }}
                    ></Image>: <Image
                     source={require('../../assets/images/homebg.png')}
                     style={{
                       width: 120,
                       height: 80,
                       borderWidth: 1,
                       borderColor: colors.textGray,
                     }}
                   ></Image>}
                   
                    {/* <Ionicons
                      name="ios-play-circle"
                      size={45}
                      style={{ position: "absolute", top: 2, left: 5 }}
                      color={"red"}
                    /> */}
                    <Text
                      style={{
                        position: "absolute",
                        color: "white",
                        paddingVertical: 5,
                        paddingHorizontal: 20,
                        backgroundColor: "red",
                        bottom: 5,
                        right: 0,
                      }}
                    >
                      Lesson #{index+1}
                    </Text>
                  </TouchableOpacity>
                  <View
                    style={{
                      paddingLeft: 15,
                      width: 240,
                      paddingVertical: 5,
                      alignSelf: "center",
                    }}
                  >
                      <TouchableOpacity onPress={props.onPress}>
                    <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                      {name}
                    </Text>
                    </TouchableOpacity>
                  </View>
                 
                  </View>
     )
}


export default CourseLessons