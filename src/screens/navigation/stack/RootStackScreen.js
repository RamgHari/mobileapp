import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from '../../signin/splash';
import SignupScreen from '../../signin/signup';
import SigninScreen from '../../signin/signin';

const RootStack = createStackNavigator();
export  const  RootStackScreen=({navigation}) =>{
  return (
   <RootStack.Navigator headerMode='none'>
       <RootStack.Screen name="Splash" component={SplashScreen}/>
       <RootStack.Screen name="Signup" component={SignupScreen}/>
       <RootStack.Screen name="Signin" component={SigninScreen}/>
   </RootStack.Navigator>
  );
}

