// import React, { Component } from "react";
// import {
//   Text,
//   View,
//   Image,
//   TouchableOpacity,
//   FlatList,
//   StyleSheet,
//   ActivityIndicator,
//   TextInput,
//   StatusBar,
// } from "react-native";
// import { Avatar } from "react-native-paper";
// import camera from "../../../assets/images/camera.png";
// import like from "../../../assets/images/like.png";
// import liked from "../../../assets/images/heart.png";
// import send from "../../../assets/images/send.png";
// import comment from "../../../assets/images/comment.png";
// import axios from "axios";
// import { WebView } from "react-native-webview";
// import Timeago from "../../../components/time";
// import StatusScreen from "../../../components/status";
// import { ScrollView } from "react-native-gesture-handler";
// import AsyncStorage from '@react-native-community/async-storage';
// import Comments from "../../../components/comments";
// import Icon from "react-native-vector-icons/Ionicons";
// import { MaterialCommunityIcons, FontAwesome5 } from "@expo/vector-icons";
// import {
//   Menu,
//   MenuOptions,
//   MenuOption,
//   MenuTrigger,
// } from "react-native-popup-menu";
// const HomeScreen = (props) => {
//   const [feed, setFeed] = React.useState([]);
//   const [error, setError] = React.useState(null);
//   const [upload, setUpload] = React.useState(
//     Math.floor(Math.random() * 100) + 1
//   );
//   const [followupload, setfollowupload] = React.useState(
//     Math.floor(Math.random() * 100) + 1
//   );
//   const [users, SetUsers] = React.useState({});
//   const [modalcmt, setmodalcmt] = React.useState(false);
//   const [postId, setPostId] = React.useState();
//   const [profile, setProfile] = React.useState("");
//   if (props.route.params?.itemId) {
//     setUpload(Math.floor(Math.random() * 100) + 2);
//   }
//   let datetime = new Date();

//   const onChange = () => {
//     setUpload(Math.floor(Math.random() * 100) + 2);
//   };

//   //uj@jd0vnEinds&0_kfsf2]vk03@kfh
//   React.useEffect(() => {
//     async function getData() {
//       setTimeout(async () => {
//         try {
//           let user = JSON.parse(await AsyncStorage.getItem("userData"));
//           SetUsers(user);
//           console.log(users.id);
//         } catch (e) {
//           console.log(e);
//         }
//       }, 1000);
//       let user = JSON.parse(await AsyncStorage.getItem("userData"));
//       axios
//         .post(
//           `https://www.allskills.in/includes/ajaxFromApp.php?request=getAllFeeds`,
//           { userId: user.id }
//         )
//         .then((res) => res.data)
//         .then(
//           (result) => {
//             setFeed(result.item);
//             //console.log("Feed","feed")
//           },
//           (error) => {
//             setError(error);
//           }
//         );
//     }
//     getData();
//   }, [props.route.params?.names, upload]);

//   const handleLike = (feedId) => {
//     let user_id = users.id;
//     let feedid = feedId;

//     axios
//       .post(
//         `https://www.allskills.in/includes/ajaxFromApp.php?request=doLikePost`,
//         { user_id: user_id, feedId: feedid, datetime: +datetime }
//       )
//       .then((res) => res.data)
//       .then(
//         (result) => {
//           //setFeed(result.item)
//           //console.log("Feed",result)
//           setUpload(Math.floor(Math.random() * 100) + 2);
//         },
//         (error) => {}
//       );
//   };

//   const sharePost = (feedId, id) => {
//     let user = users.id;
//     let data = {
//       user_id: user,
//       message: "",
//       type: "shared",
//       value: feedId,
//       from_id: id,
//       urlData: "",
//       time: +datetime,
//     };
//     axios
//       .post(
//         `https://www.allskills.in/includes/ajaxFromApp.php?request=sharePost`,
//         data
//       )
//       .then((res) => res.data)
//       .then(
//         (result) => {
//           //setFeed(result.item)
//           //console.log("Feed",result)
//           setUpload(Math.floor(Math.random() * 100) + 2);
//         },
//         (error) => {}
//       );
//   };

//   const handleComment = (id) => {
//     setPostId(id);
//     setmodalcmt(true);
//   };

//   const closeModal = () => {
//     setmodalcmt(false);
//   };
//   const submitComment = (cmt) => {
//     let user = users.id;
//     let data = {
//       user_id: user,
//       commentContent: cmt,
//       feedId: postId,
//       datetime: +datetime,
//     };
//     axios
//       .post(
//         `https://www.allskills.in/includes/ajaxFromApp.php?request=commentPost`,
//         data
//       )
//       .then((res) => res.data)
//       .then(
//         (result) => {
//           console.log("Feed", result);
//           setmodalcmt(false);
//           setUpload(Math.floor(Math.random() * 100) + 3);
//         },
//         (error) => {}
//       );
//   };

//   const deletePost = (feedId, user_id) => {
//     let data = { feedId: feedId, user_id: user_id, datetime: +datetime };
//     axios
//       .post(
//         `https://www.allskills.in/includes/ajaxFromApp.php?request=deletePost`,
//         data
//       )
//       .then((res) => res.data)
//       .then(
//         (result) => {
//           console.log("res", result);
//           setFeed(feed.filter((item) => item.id !== feedId));
//         },
//         (error) => {}
//       );
//   };

//   if (error) {
//     return (
//       <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
//         <Text style={{ fontSize: 18 }}>
//           Error fetching data... Check your network connection!
//         </Text>
//       </View>
//     );
//   }

//   const RenderProfile = ({ user }) => {
//     const feedId = user.id;
//     const user_id = user.user.id;
//     return (
//       <View style={styles.feedItemHeader}>
//         <TouchableOpacity
//           onPress={() =>
//             user.user.id != users.id
//               ? props.navigation.navigate("UserProfile", user.user)
//               : props.navigation.navigate("Profile", user.user)
//           }
//         >
//           <View style={styles.imgInfo}>
//             <Avatar.Image source={{ uri: user.user.image }} size={40} />
//             <View style={styles.userInfo}>
//               <Text style={styles.name}> {user.user.first_name}</Text>
//               <TouchableOpacity
//                 onPress={() => props.navigation.navigate("SinglePost", user)}
//               >
//                 <Timeago time={user.time} />
//               </TouchableOpacity>
//             </View>
//           </View>
//         </TouchableOpacity>
//         {user.user.id == users.id ? (
//           <Menu style={styles.popupMenu}>
//             <MenuTrigger>
//               <MaterialCommunityIcons name="dots-vertical" size={26} />
//             </MenuTrigger>
//             <MenuOptions>
//               <MenuOption
//                 onSelect={() => deletePost(feedId, user_id)}
//                 text="Delete"
//               />
//               {/* <MenuOption onSelect={() => alert(`Delete`)} >
//           <Text style={{color: 'red'}}>Delete</Text>
//         </MenuOption>
//         <MenuOption onSelect={() => alert(`Not called`)} disabled={true} text='Disabled' /> */}
//             </MenuOptions>
//           </Menu>
//         ) : null}
//       </View>
//     );
//   };
//   return (
//     <View style={styles.container}>
//       <FlatList
//         ListHeaderComponent={<StatusScreen onChange={(e) => onChange(e)} />}
//         data={feed.slice(0, 20)}
//         keyExtractor={(post) => post.id}
//         renderItem={({ item, index }) => (
//           <View style={styles.feedItem}>
//             {item.user.id != users.id ? (
//               <RenderProfile user={item} />
//             ) : (
//               <RenderProfile user={item} />
//             )}

//             <View>
//               {item.type == "picture" ? (
//                 <View>
//                   <Image
//                     style={styles.feedImage}
//                     source={{ uri: `${item.post_image}` }}
//                   />
//                   <Text style={styles.feedText}>{item.message}</Text>
//                 </View>
//               ) : item.type == "video" ? (
//                 <Video
//                   source={{
//                     uri: `https://www.allskills.in/uploads/course/lesson/video/${item.video_file}`,
//                   }}
//                   rate={1.0}
//                   style={styles.feedImage}
//                   isMuted={false}
//                   resizeMode="cover"
//                   shouldPlay={true}
//                   isLooping={false}
//                   useNativeControls
//                 />
//               ) : item.type == "youtube" ? (
//                 <WebView
//                   style={styles.feedImage}
//                   source={{
//                     uri: "https://www.youtube.com/embed/" + item.value,
//                   }}
//                 />
//               ) : item.type == "shared" ? (
//                 item.sharedFeedDetails.details.type == "picture" ? (
//                   <View>
//                     <Image
//                       style={styles.feedImage}
//                       source={{ uri: `${item.sharedFeedDetails.post_image}` }}
//                     />
//                     <Text style={styles.feedText}>
//                       {item.sharedFeedDetails.details.message}
//                     </Text>
//                   </View>
//                 ) : item.sharedFeedDetails.details.type == "youtube" ? (
//                   <WebView
//                     style={styles.feedImage}
//                     source={{
//                       uri:
//                         "https://www.youtube.com/embed/" +
//                         item.sharedFeedDetails.details.value,
//                     }}
//                   />
//                 ) : (
//                   <Text style={styles.feedText}>
//                     {item.sharedFeedDetails.details.message}
//                   </Text>
//                 )
//               ) : (
//                 <Text style={styles.feedText}>{item.message}</Text>
//               )}
             
//             </View>

//             <View style={styles.feedItemFooter}>
//               <View style={styles.actions}>
//                 <TouchableOpacity
//                   style={styles.action}
//                   onPress={() => handleLike(item.id)}
//                 >
//                   {item.userLikedStatus ? (
//                     <Image source={liked} />
//                   ) : (
//                     <Image source={like} />
//                   )}
//                 </TouchableOpacity>
//                 <TouchableOpacity
//                   style={styles.action}
//                   onPress={() => handleComment(item.id)}
//                 >
//                   <Image source={comment} />
//                 </TouchableOpacity>

//                 {users.id == item.user.id ? null : (
//                   <TouchableOpacity
//                     style={styles.action}
//                     onPress={() => sharePost(item.id, item.user.id)}
//                   >
//                     <Image source={send} />
//                   </TouchableOpacity>
//                 )}
//               </View>
//             </View>
//             <TouchableOpacity
//               onPress={() => props.navigation.navigate("SinglePost", item)}
//             >
//               {item.feedComments != "" ? (
//                 <FlatList
//                   ListHeaderComponent={
//                     <View style={{ marginLeft: 20 }}>
//                       <Text style={styles.name}>Comments</Text>
//                     </View>
//                   }
//                   data={item.feedComments.slice(0, 2)}
//                   keyExtractor={(comment) => comment.id}
//                   renderItem={({ item, index }) => (
//                     <View style={styles.commentcontainer}>
//                       <View style={styles.imgInfo}>
//                         <Avatar.Image
//                           source={{ uri: item.userDetails.image }}
//                           size={30}
//                         />

//                         <View style={styles.userInfo}>
//                           <Text style={styles.comment}>
//                             {" "}
//                             {item.userDetails.first_name}
//                           </Text>
//                           {/* <Timeago time={item.time}/>   */}
//                           <Text> {item.comment}</Text>
//                         </View>
//                       </View>
//                     </View>
//                   )}
//                 />
//               ) : null}
//             </TouchableOpacity>

//             <View
//               style={{
//                 borderBottomColor: "#ddd",
//                 borderBottomWidth: 1,
//               }}
//             />
//           </View>
//         )}
//       />

//       {modalcmt ? (
//         <Comments
//           closeModal={closeModal}
//           handlesubmitComment={(cmt) => submitComment(cmt)}
//         />
//       ) : null}
//     </View>
//   );
// };
// export default HomeScreen;
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     marginTop: StatusBar.currentHeight || 0,
//   },

//   commentcontainer: {
//     margin: 10,
//     paddingTop: 10,
//     paddingRight: 10,
//     paddingLeft: 10,
//     //   borderColor: '#009387',
//     //   borderWidth: 1,
//     //  // borderStyle:"dashed",
//     borderRadius: 10,
//     backgroundColor: "#fff",
//   },
//   input: {
//     flex: 1,
//   },
//   feedItem: {
//     marginTop: 20,
//   },
//   imgInfo: {
//     flexDirection: "row",
//   },
//   userInfo: {
//     marginLeft: 10,
//     flexDirection: "column",
//     justifyContent: "center",
//   },
//   feedItemHeader: {
//     paddingHorizontal: 15,
//     flexDirection: "row",
//     justifyContent: "space-between",
//   },
//   name: {
//     fontSize: 14,
//     color: "#000",
//   },
//   comment: {
//     fontSize: 14,
//     color: "#000",
//     fontFamily: "Regular",
//   },
//   place: {
//     fontSize: 12,
//     color: "#666",
//   },
//   feedImage: {
//     width: "100%",
//     height: 250,
//     marginVertical: 15,
//   },
//   feedText: {
//     width: "100%",
//     marginVertical: 15,
//     marginLeft: 8,
//     fontFamily: "Medium",
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   feedItemFooter: {
//     paddingHorizontal: 15,
//     marginBottom: 15,
//     borderBottomColor: "#333",
//   },
//   actions: {
//     flexDirection: "row",
//   },
//   action: {
//     marginRight: 15,
//   },
//   likes: {
//     marginTop: 15,
//     fontWeight: "bold",
//     color: "#000",
//   },
//   description: {
//     lineHeight: 18,
//     color: "#000",
//   },
//   hashtags: {
//     color: "#7159c1",
//   },
//   last: {
//     marginBottom: 15,
//   },
// });
