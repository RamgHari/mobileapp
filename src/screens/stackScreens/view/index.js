import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Alert,
  Dimensions
} from "react-native";
import { colors } from "../../../assets/color/style";
import axios from 'axios';
import { Ionicons, Feather } from "@expo/vector-icons";
import CourseChapters from "../../../components/coursechapter";
import { WebView } from "react-native-webview";
import { useSelector} from 'react-redux';
const { width } = Dimensions.get("screen");

const CourseView = (props) => {
  
  const course=props.route.params.course;
  const [lists,setLists]=React.useState([])
  const users =useSelector(state => state.userReducer.user)
  //console.log("dd",props.route.params)
React.useEffect(() => {
  async function getData() {  
      axios.post(`https://www.allskills.in/includes/ajaxFromApp.php?request=getChaptersAndLessons`,{courseId:course.id,userId:users.id})
      .then((res)  =>  res.data)
    .then(
      (result) => {
         setLists(result.data)
         
         //alert(props.route.params.update)
      },
      (error) => {
        alert(error);
      } 
    );
  }
  getData();
}, [props.route.params?.update])
  
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={{ paddingTop: 12, paddingLeft: 15 }}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Ionicons name="md-arrow-back" size={24} color="#d8d9d8"></Ionicons>
          </TouchableOpacity>
        </View>
        <View
          style={{ flex: 1.5, alignContent: "center", alignItems: "center" }}
        >
          <Text
            style={{
              color: "white",
              alignSelf: "center",
              marginTop: 12,
              fontSize: 15,
              fontWeight: "900",
            }}
          >
            {course.name}
          </Text>
        </View>
     
      </View>
      {course.external_url == '' ?
      <ScrollView>
        <View>
          <ImageBackground source={{uri:`https://www.allskills.in/uploads/course/${course.image}`}}
          style={{ width: "100%", height: 200 }}
          />
        </View>
        <View style={{ marginVertical: 10 }}>
          <Text
            style={{
              fontSize: 14,
              color: colors.textGray,
              paddingLeft: 15,
              paddingTop: 10,
              fontWeight: "800",
            }}
          >
            {course.description}
          </Text>
          <Text
            style={{ fontSize: 14, color: colors.textGray, paddingLeft: 15 }}
          >
            By {course.userDetails.first_name +' '+ course.userDetails.last_name}
          </Text>
          {/*<View
            style={{ flexDirection: "row", paddingLeft: 15, paddingTop: 5 }}
          >
            <Ionicons
              name="ios-play-circle"
              size={45}
              color='#009387' 
            />
             <Text
              style={{
                fontSize: 18,
                alignSelf: "center",
                paddingLeft: 10,
                color: colors.textGray ,
              }}>
              Resume Course
            </Text> 
          </View>*/}
        </View>

        <View style={{ marginBottom: 20 }}>
          <Text style={{ fontSize: 18, paddingLeft: 15 }}>Lessons</Text>
          <View
            style={{
              borderBottomColor: colors.textGray,
              borderBottomWidth: 0.5,
              paddingTop: 10,
            }}
          ></View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              paddingTop: 10,
            }}
          >
            <Text
              style={{
                fontSize: 18,
                color: colors.textGray,
                paddingLeft: 15,
                alignSelf: "center",
              }}
            >
              Beginner
            </Text>
            <Text
              style={{
                fontSize: 15,
                color: "white",
                paddingVertical: 10,
                paddingHorizontal: 20,
                marginRight: 15,
                borderRadius: 20,
                backgroundColor: '#009387' ,
              }}
            >
              Chapter 1-{lists.length}
            </Text>
          </View>
        </View>

        <View>
        {
                    lists.length>0?lists.map((item,index)=>{ 
                        return(<CourseChapters  index={index} users={users}  key={item.id}   lessons={item}/>)
                    })
        :<Text></Text>} 
        </View>
      </ScrollView>:<WebView
                    source={{uri:course.external_url}}
                    style={{ width: width, height: 3000 }}
                  />}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  header: {
    backgroundColor: '#009387' ,
    flexDirection: "row",
    height: 50,
    // marginTop: 20,
    borderBottomColor: "black",
    borderBottomWidth: 1,
  },
  imageBackground: {
    height: 280,
    justifyContent: "flex-end",
  },
  imageContainer: {
    margin: 5,
    flexDirection: "row",
    backgroundColor: "white",
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
  },
  image: {
    width: 180,
    height: 180,
    margin: 10,
    borderRadius: 90,
    borderColor: "red",
    borderWidth: 3,
  },
  textContainer: {
    marginLeft: 5,
    width: "45%",
    marginTop: 10,
  },
});

export default CourseView;
