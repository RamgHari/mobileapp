import React, { Component } from 'react';
import { Text } from 'react-native';''
import moment from 'moment';

class Timeago extends Component {

    constructor(props) {
        super(props);
        this.date = parseInt(props.time);
        this.dateObject = new Date(this.date);
        this.time = moment( this.dateObject || moment.now() ).fromNow();
    }

    render() {
        
        return (
            <Text style={{marginLeft:2,fontSize: 14,
                color: '#000',
                fontFamily:'Regular'}}>{this.time }</Text>
        );
    }
}

export default Timeago;