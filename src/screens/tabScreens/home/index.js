import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  TextInput,
  StatusBar,
} from "react-native";
import { Avatar } from "react-native-paper";
import like from "../../../assets/images/like.png";
import liked from "../../../assets/images/heart.png";
import send from "../../../assets/images/send.png";
import comment from "../../../assets/images/comment.png";
import axios from "axios";
import { WebView } from "react-native-webview";
import Timeago from "../../../components/time";
import StatusScreen from "../../../components/status";
import Comments from "../../../components/comments";
import { MaterialCommunityIcons, FontAwesome5 } from "@expo/vector-icons";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from "react-native-popup-menu";
import { useSelector,useDispatch} from 'react-redux';
import {onFetchFeed,onDeleteFeed,onLikeFeed,onSubmitComment,onSharePost} from '../../../redux'
import { useIsFocused } from "@react-navigation/native";

const HomeScreen = (props) => {
  const dispatch = useDispatch()
  const isFocused = useIsFocused();
  const users=useSelector(state => state.userReducer.user)
  const feed=useSelector(state => state.userReducer.feed)
  const openModal=useSelector(state => state.userReducer.openModal)
  const postId=useSelector(state => state.userReducer.cmtPostId)
  const error =useSelector(state => state.userReducer.appError)
  

  
// 

  //uj@jd0vnEinds&0_kfsf2]vk03@kfh
  // Index-HpnAKFvdzw-sLEKUlvg4e
  //Home-cUKouAwQFzNcUNQWoSw5C



  React.useEffect(() => {
    dispatch(onFetchFeed(users.id))
  }, [isFocused]);

  const handleComment = (id) => {
  
    dispatch({type:'OPEN_MODAL',key:openModal,postId:id})
  };

  const closeModal = () => {
    
    dispatch({type:'OPEN_MODAL',key:openModal,postId:''})
  };

  if (error) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text style={{ fontSize: 18 }}>
          Error fetching data... Check your network connection!
        </Text>
      </View>
    );
  }

  const RenderProfile = ({ user,index }) => {
    const feedId = user.id;
    const user_id = user.user.id;
    return (
      <View style={styles.feedItemHeader}>
        <TouchableOpacity
          onPress={() =>
            user.user.id != users.id
              ? props.navigation.navigate("UserProfile", user.user)
              : props.navigation.navigate("Profile", user.user)
          }
        >
          <View style={styles.imgInfo}>
            <Avatar.Image source={{ uri: user.user.image }} size={40} />
            <View style={styles.userInfo}>
              <Text style={styles.name}> {user.user.first_name}</Text>
              <TouchableOpacity
                onPress={() => props.navigation.navigate("SinglePost", index)}
              >
                <Timeago time={user.time} />
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
        {user.user.id == users.id ? (
          <Menu style={styles.popupMenu}>
            <MenuTrigger>
              <MaterialCommunityIcons name="dots-vertical" size={26} />
            </MenuTrigger>
            <MenuOptions>
              <MenuOption
                onSelect={() => dispatch(onDeleteFeed(feedId, user_id))}
                text="Delete"
              />
              {/* <MenuOption onSelect={() => alert(`Delete`)} >
          <Text style={{color: 'red'}}>Delete</Text>
        </MenuOption>
        <MenuOption onSelect={() => alert(`Not called`)} disabled={true} text='Disabled' /> */}
            </MenuOptions>
          </Menu>
        ) : null}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <FlatList
        ListHeaderComponent={<StatusScreen onChange={(e) => onChange(e)} />}
        data={feed.slice(0, 20)}
        keyExtractor={(post) => post.id}
        renderItem={({ item, index }) => (
          <View style={styles.feedItem}>
            {item.user.id != users.id ? (
              <RenderProfile user={item} index={index} />
            ) : (
              <RenderProfile user={item} index={index}/>
            )}

            <View>
              {item.type == "picture" ? (
                <View>
                  <Image
                    style={styles.feedImage}
                    source={{ uri: `${item.post_image}` }}
                  />
                  <Text style={styles.feedText}>{item.message}</Text>
                </View>
              ) : item.type == "video" ? (
                <Video
                  source={{
                    uri: `https://www.allskills.in/uploads/course/lesson/video/${item.video_file}`,
                  }}
                  rate={1.0}
                  style={styles.feedImage}
                  isMuted={false}
                  resizeMode="cover"
                  shouldPlay={true}
                  isLooping={false}
                  useNativeControls
                />
              ) : item.type == "youtube" ? (
                <WebView
                  style={styles.feedImage}
                  source={{
                    uri: "https://www.youtube.com/embed/" + item.value,
                  }}
                />
              ) : item.type == "shared" ? (
                item.sharedFeedDetails.details.type == "picture" ? (
                  <View>
                    <Image
                      style={styles.feedImage}
                      source={{ uri: `${item.sharedFeedDetails.post_image}` }}
                    />
                    <Text style={styles.feedText}>
                      {item.sharedFeedDetails.details.message}
                    </Text>
                  </View>
                ) : item.sharedFeedDetails.details.type == "youtube" ? (
                  <WebView
                    style={styles.feedImage}
                    source={{
                      uri:
                        "https://www.youtube.com/embed/" +
                        item.sharedFeedDetails.details.value,
                    }}
                  />
                ) : (
                  <Text style={styles.feedText}>
                    {item.sharedFeedDetails.details.message}
                  </Text>
                )
              ) : (
                <Text style={styles.feedText}>{item.message}</Text>
              )}
             
            </View>

            <View style={styles.feedItemFooter}>
              <View style={styles.actions}>
                <TouchableOpacity
                  style={styles.action}
                  onPress={() => dispatch(onLikeFeed(item.id,users.id))}
                >
                  {item.userLikedStatus ? (
                    <Image source={liked} />
                  ) : (
                    <Image source={like} />
                  )}
                  {/* <Text>{item.from_id}</Text> */}
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.action}
                  onPress={() => handleComment(item.id)}
                >
                  <Image source={comment} />
                </TouchableOpacity>

                {users.id == item.user.id ? null : (
                  <TouchableOpacity
                    style={styles.action}
                    onPress={() => dispatch(onSharePost(item.id, item.user.id,users.id))}
                  >
                    <Image source={send} />
                  </TouchableOpacity>
                )}
              </View>
            </View>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("SinglePost", index)}
            >
              {item.feedComments != "" ? (
                <FlatList
                  ListHeaderComponent={
                    <View style={{ marginLeft: 20 }}>
                      <Text style={styles.name}>Comments</Text>
                    </View>
                  }
                  data={item.feedComments.slice(0, 2)}
                  keyExtractor={(comment) => comment.id}
                  renderItem={({ item, index }) => (
                    <View style={styles.commentcontainer}>
                      <View style={styles.imgInfo}>
                        <Avatar.Image
                          source={{ uri: item.userDetails.image }}
                          size={30}
                        />

                        <View style={styles.userInfo}>
                          <Text style={styles.comment}>
                            {" "}
                            {item.userDetails.first_name}
                          </Text>
                          {/* <Timeago time={item.time}/>   */}
                          <Text> {item.comment}</Text>
                        </View>
                      </View>
                    </View>
                  )}
                />
              ) : null}
            </TouchableOpacity>

            <View
              style={{
                borderBottomColor: "#ddd",
                borderBottomWidth: 1,
              }}
            />
          </View>
        )}
      />

      {openModal ? (
        <Comments
          closeModal={closeModal}
          //handlesubmitComment={(cmt) => submitComment(cmt)}
          handlesubmitComment={(cmt) => dispatch(onSubmitComment(postId,users.id,cmt))}
        />
      ) : null}
    </View>
  );
};
export default HomeScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },

  commentcontainer: {
    margin: 10,
    paddingTop: 10,
    paddingRight: 10,
    paddingLeft: 10,
    //   borderColor: '#009387',
    //   borderWidth: 1,
    //  // borderStyle:"dashed",
    borderRadius: 10,
    backgroundColor: "#fff",
  },
  input: {
    flex: 1,
  },
  feedItem: {
    marginTop: 20,
  },
  imgInfo: {
    flexDirection: "row",
  },
  userInfo: {
    marginLeft: 10,
    flexDirection: "column",
    justifyContent: "center",
  },
  feedItemHeader: {
    paddingHorizontal: 15,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  name: {
    fontSize: 14,
    color: "#000",
  },
  comment: {
    fontSize: 14,
    color: "#000",
    fontFamily: "Regular",
  },
  place: {
    fontSize: 12,
    color: "#666",
  },
  feedImage: {
    width: "100%",
    height: 250,
    marginVertical: 15,
  },
  feedText: {
    width: "100%",
    marginVertical: 15,
    marginLeft: 8,
    fontFamily: "Medium",
    alignItems: "center",
    justifyContent: "center",
  },
  feedItemFooter: {
    paddingHorizontal: 15,
    marginBottom: 15,
    borderBottomColor: "#333",
  },
  actions: {
    flexDirection: "row",
  },
  action: {
    marginRight: 15,
  },
  likes: {
    marginTop: 15,
    fontWeight: "bold",
    color: "#000",
  },
  description: {
    lineHeight: 18,
    color: "#000",
  },
  hashtags: {
    color: "#7159c1",
  },
  last: {
    marginBottom: 15,
  },
});
