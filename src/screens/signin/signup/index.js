import * as React from 'react';
import { Button,Image, View, Text, StyleSheet,Dimensions,TouchableOpacity,TextInput ,StatusBar,Alert,ActivityIndicator} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {LinearGradient} from 'expo-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Feather from 'react-native-vector-icons/Feather'
import * as Facebook from "expo-facebook";
import * as Google from 'expo-google-app-auth';
import styles from './style.js';
import Constants from 'expo-constants';
import SocialButton from '../../../components/SocialButton';
import {onUserSignUp} from '../../../redux'
import {useSelector,useDispatch} from 'react-redux';
import firebase from 'firebase';
// import firebaseConfig from './config';
//firebase.initializeApp(firebaseConfig);
const  SignupScreen=(props) =>{
  const isInExpo = Constants.appOwnership === 'expo';
  const emailError=useSelector(state => state.userReducer.emailError)
  const SignupError=useSelector(state => state.userReducer.SignupError)
  const dispatch = useDispatch()
    const [data, setData] = React.useState({
        username: '',
        password: '',
        email:'',
        firstname:'',
        lastname:'',
        check_emailInputChange: false,
        check_textInputChange: false,
        check_firstInputChange: false,
        check_lastInputChange: false,
        isValidUser:true,
        isValidFirstName:true,
        isValidLastName:true,
        isValidEmail:true,
        isValidPassword:true,

        secureTextEntry: true
        
    });

    

    const textInputChange=(val)=>{
        if( val.trim().length >= 4 ) {
            setData({
                ...data,
                username: val,
                check_textInputChange: true,
                isValidUser:true
            });
        } else {
            setData({
                ...data,
                username: val,
                check_textInputChange: false,
                isValidUser: false,
            });
        }
    }
    const firstInputChange=(val)=>{
        if( val.trim().length >= 1 ) {
            setData({
                ...data,
                firstname: val,
                check_firstInputChange: true,
                isValidFirstName:true
            });
        } else {
            setData({
                ...data,
                firstname: val,
                check_firstInputChange: false,
                isValidFirstName:false
            });
        }
    }

    const lastInputChange=(val)=>{
        if( val.trim().length >= 1 ) {
            setData({
                ...data,
                lastname: val,
                check_lastInputChange: true,
                isValidLastName:true
            });
        } else {
            setData({
                ...data,
                lastname: val,
                check_lastInputChange: false,
                isValidLastName:false
            });
        }
    }
    
    const emailInputChange=(val)=>{
        if( val.trim().length >= 1) {
            setData({
                ...data,
                email: val,
                check_emailInputChange: true,
                isValidEmail:true
            });
        } else {
            setData({
                ...data,
                email: val,
                check_emailInputChange: false,
                isValidEmail: false,
            });
        }
    }
    const handlePasswordChange=(val)=>{
        if( val.trim().length >= 8 ) {
        setData({
            ...data,
            password:val,
            isValidPassword:true
        })
        }else{
            setData({
                ...data,
                password:val,
                isValidPassword:false
            })
        }
    }
    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }


    const handleValidUser=(val)=>{
        if(val.trim().length>=4){
            setData({
                ...data,
                isValidUser:true
            })
        }else{
            setData({
                ...data,
                isValidUser:false
            })
        }
    }

    const handleValidFirst=(val)=>{
        if(val.trim().length>=1){
            setData({
                ...data,
                isValidFirstName:true
            })
        }else{
            setData({
                ...data,
                isValidFirstName:false
            })
        }
    }

    const handleValidLast=(val)=>{
        if(val.trim().length>=1){
            setData({
                ...data,
                isValidLastName:true
            })
        }else{
            setData({
                ...data,
                isValidLastName:false
            })
        }
    }

    const handleValidEmail=(val)=>{
        if(val.trim().length>=1){
            setData({
                ...data,
                isValidEmail:true
            })
        }else{
            setData({
                ...data,
                isValidEmail:false
            })
        }
    }

    

  return (
    <View style={styles.container}>
        <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <View animation="fadeInUpBig" style={styles.footer}>
        <View style={styles.imgcontainer}>
        <Image
        source={require('../../../assets/images/icon.png')}
        style={styles.logo}
      />
      </View>
            {/* <Text style={styles.text_footer}></Text>
            <View style={styles.action}>
                <FontAwesome
                    name="user-o"
                    color="#05375a"
                    size={20}
                />
                <TextInput
                    placeholder="Enter Your Username"
                    style={styles.textInput}
                    autoCapitalize = 'none'
                    onChangeText={(val) => textInputChange(val)}
                    value={data.username}
                    onEndEditing={(e)=>handleValidUser(e.nativeEvent.text)}
                    />
                {data.check_textInputChange ?
                <Animatable.View
                    animation="bounceIn">
                    <Feather
                        name="check-circle"
                        color="green"
                        size={20}
                    />
                </Animatable.View>:null}
            </View>
            { data.isValidUser ? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>UserName must be 4 characters long.</Text>
            </Animatable.View>
            } */}


            <Text style={styles.text_footer}></Text>
            <View style={styles.action}>
            <FontAwesome
                    name="user-o"
                    color="#05375a"
                    size={20}
                />
                <TextInput
                    placeholder="Enter Your FirstName"
                    style={styles.textInput}
                    autoCapitalize = 'none'
                    onChangeText={(val) => firstInputChange(val)}
                    value={data.firstname}
                    onEndEditing={(e)=>handleValidFirst(e.nativeEvent.text)}
                    />
                {data.check_emailInputChange ?
                <Animatable.View
                    animation="bounceIn">
                    <Feather
                        name="check-circle"
                        color="green"
                        size={20}
                    />
                </Animatable.View>:null}
            </View>
           
            { data.isValidFirstName ? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>FrstName must be 1 characters long.</Text>
            </Animatable.View>
            }

            <Text style={styles.text_footer}></Text>
            <View style={styles.action}>
            <FontAwesome
                    name="user-o"
                    color="#05375a"
                    size={20}
                />
                <TextInput
                    placeholder="Enter Your LastName"
                    style={styles.textInput}
                    autoCapitalize = 'none'
                    onChangeText={(val) => lastInputChange(val)}
                    value={data.lastname}
                    onEndEditing={(e)=>handleValidLast(e.nativeEvent.text)}
                    />
                {data.check_lastInputChange ?
                <Animatable.View
                    animation="bounceIn">
                    <Feather
                        name="check-circle"
                        color="green"
                        size={20}
                    />
                </Animatable.View>:null}
            </View>
           
            { data.isValidLastName? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>Last must be 1 characters long.</Text>
            </Animatable.View>
            }


            <Text style={styles.text_footer}></Text>
            <View style={styles.action}>
                <MaterialIcons
                    name="email"
                    color="#05375a"
                    size={20}
                />
                <TextInput
                    placeholder="Enter Your Email"
                    style={styles.textInput}
                    autoCapitalize = 'none'
                    onChangeText={(val) => emailInputChange(val)}
                    value={data.email}
                    onEndEditing={(e)=>handleValidEmail(e.nativeEvent.text)}
                    />
                {data.check_emailInputChange ?
                <Animatable.View
                    animation="bounceIn">
                    <Feather
                        name="check-circle"
                        color="green"
                        size={20}
                    />
                </Animatable.View>:null}
            </View>
            { data.isValidEmail ? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>Email must be 4 characters long.</Text>
            </Animatable.View>
            }
            
            { emailError=='' ? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>{emailError}</Text>
            </Animatable.View>
            }
            <Text style={[styles.text_footer]}></Text>
            <View style={styles.action}>
                <FontAwesome
                    name="lock"
                    color="#05375a"
                    size={20}
                />
                <TextInput
                    placeholder="Enter Your Password"
                    secureTextEntry={data.secureTextEntry?true:false}
                    style={styles.textInput}
                    value={data.password}
                    onChangeText={(val) => handlePasswordChange(val)}
                    />
                <TouchableOpacity onPress={updateSecureTextEntry}>
                    <Feather
                        name={data.secureTextEntry?'eye-off':'eye'}
                        color="gray"
                        size={20}
                        />
                </TouchableOpacity>
            </View>
            { data.isValidPassword ? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>Password must be 8 characters long.</Text>
            </Animatable.View>
            }
             
             { SignupError=='' ? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>{SignupError}</Text>
            </Animatable.View>
            }


            <View style={styles.button}>
                <TouchableOpacity
                    style={styles.signIn}

                    onPress={() => {dispatch(onUserSignUp(data.email,data.firstname,data.lastname,data.password))}} >
                <LinearGradient
                    colors={['#08d4c4', '#01ab9d']}
                    style={styles.signIn}>
                    <Text style={[styles.textSign, {color:'#fff'}]}>Sign Up</Text>
                </LinearGradient>
                </TouchableOpacity>

         


                <TouchableOpacity
                    onPress={() => props.navigation.navigate('Signin')}
                    style={[styles.signIn, {
                        borderColor: '#009387',
                        borderWidth: 1,
                        marginTop: 30
                    }]}>
                    <Text style={[styles.textSign, {color: '#009387'}]}>Sign in</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
  );
}
// const mapStateToProps=(state)=>({
//   userReducer:state.userReducer
// })
// const SigninScreen=connect(mapStateToProps,{onUserLogin})(_LoginScreen)
export default SignupScreen;
