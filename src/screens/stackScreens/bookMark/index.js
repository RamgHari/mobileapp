import * as React from 'react';
import { Button, View, Text, StyleSheet } from 'react-native';
import styles from './style.js';



const  BookmarkScreen=() =>{
  return (
    <View style={styles.container}>
      <Text>Bookmark Screen</Text>
      <Button
        title="Click Here"
        onPress={() => alert('Clicked')}
      />
    </View>
  );
}

export default BookmarkScreen;

