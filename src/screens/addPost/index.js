import React, {useState,useEffect} from 'react';
import {
  View,
  Text,
  Platform,
  StyleSheet,
  Alert,
  ActivityIndicator,
} from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';

import {
  InputField,
  InputWrapper,
  AddImage,
  SubmitBtn,
  SubmitBtnText,
  StatusWrapper,
} from '../../assets/style/AddPost';
import { HeaderBackground } from '@react-navigation/stack';
import { useSelector} from 'react-redux';
const AddPostScreen = (props) => {
  const [image, setImage] = useState(null);
  const [content, setContent] = useState('');
  const [updated,setImageUpdate]=useState(false)
  const [uploading, setUploading] = useState(false);
  const [transferred, setTransferred] = useState(0);
  const [user,setUser]=useSelector(state => state.userReducer.user)
  useEffect(() => {
    
    (async () => {
          
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
      setImageUpdate(true)
    }
  };





  const submitPost = async () => {
    const formData = new FormData();

        const uri = image;
        let dateTime = new Date();
        setUploading(true);
        if(uri){
          if(uri &&  updated ){
            const uriParts = uri.split('.');
            const fileType = uriParts[uriParts.length - 1];
            formData.append('value', {
              uri,
              name: `photo.${fileType}`,
              type: `image/${fileType}`,
            });
          }else{
            formData.append('value', content);
          }
          formData.append('type', "picture");
        }else{
          formData.append('type', "");
          formData.append('value', "");
        }

        let actionRequest = "postFeedFromApp";

        formData.append('feedContent', content);
        formData.append('userId', user.id);
        formData.append('tag', "");
        formData.append('datetime', +dateTime);

        if( content != '' ){
          fetch('https://www.allskills.in/includes/ajaxFromApp.php?request='+actionRequest,{
            method : 'POST',
            headers : {
              Accept : 'application/json',
              'Content-Type' : 'multipart/form-data'
            },
            body : formData,
          }).then((res) => {
            setUploading(false);
            setTransferred(100);
            setContent('');
            setImage(null);
            props.navigation.navigate('Home',{upload:Math.floor(Math.random() * 100) + 1})
            })
          }else{
            setUploading(false);
            alert("Check Input");
          }
  }
  return (
    <View style={styles.container}>
      <InputWrapper>
        {image != null ? <AddImage source={{uri: image}} /> : null}
        
        <InputField
          placeholder="What's on your mind?"
          multiline
          numberOfLines={4}
          onChangeText={(val)=>setContent(val)}
        />
        {uploading ? (
          <StatusWrapper>
            <Text>Uploading!</Text>
            <ActivityIndicator size="large" color="#0000ff" />
          </StatusWrapper>
        ) : (
          <SubmitBtn onPress={submitPost}>
            <SubmitBtnText>Post</SubmitBtnText>
          </SubmitBtn>
        )}
      </InputWrapper>
      <ActionButton buttonColor="#009387">
        {/* <ActionButton.Item
          buttonColor="#9b59b6"
          title="Take Photo"
          onPress={pickCamera}>
          <Icon name="ios-camera" style={styles.actionButtonIcon} />
        </ActionButton.Item> */}
        <ActionButton.Item
          buttonColor="#3498db"
          title="Choose Photo"
          onPress={pickImage}>
          <Icon name="ios-camera" style={styles.actionButtonIcon} />
        </ActionButton.Item>
      </ActionButton>
    </View>
  );
};

export default AddPostScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});