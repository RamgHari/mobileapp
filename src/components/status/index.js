import React, {useState,useEffect} from 'react';
import {
  View,
  Text,
  Platform,
  StyleSheet,
  Alert,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import * as ImagePicker from 'expo-image-picker';
import {
  InputField,
  InputWrapper,
  AddImage,
  SubmitBtn,
  SubmitBtnText,
  StatusWrapper,
} from '../../assets/style/AddPost';
import { useSelector,useDispatch} from 'react-redux';
import {onSubmitPost} from '../../redux'
const StatusScreen = (props) => {
  const dispatch = useDispatch()
  const image=useSelector(state => state.userReducer.image)
  const content= useSelector(state => state.userReducer.content)
  const updated=useSelector(state => state.userReducer.updated)
  const uploading = useSelector(state => state.userReducer.uploading)
  const users=useSelector(state => state.userReducer.user)
  useEffect(() => {
    (async () => {
         
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    if (!result.cancelled) {
      dispatch({type:'SET_IMAGE',value:result.uri})
      dispatch({type:'SET_IMAGE_UPLOAD'})
    }
  };
  
  return (
    <View style={styles.container}>
     
        {image != null ? <AddImage source={{uri: image}} /> : null}
        <View>
        <InputField
          placeholder="What's on your mind?"
          value={content}
          multiline
          numberOfLines={4}
          onChangeText={(val)=> dispatch({type:'SET_CONTENT',value:val})}
        />
        </View>
        <View style={styles.icons}>
          <Icon name="ios-camera"  onPress={pickImage} size={30}  />
        {
        uploading ? (
          <StatusWrapper>
            <Text>Uploading!</Text>
            <ActivityIndicator size="large" color="#0000ff" />
          </StatusWrapper>
        ) : 
        (
            content || image ? 
            <SubmitBtnText onPress={() => dispatch(onSubmitPost(image,content,users.id,updated))}>
            post
            </SubmitBtnText>
          :<Text></Text>
        )} 
         </View>
    </View>
  );
};

export default StatusScreen;

const styles = StyleSheet.create({
  container: {
    margin:10,
    paddingTop:10,
    paddingRight:10,
    paddingLeft:10,
    borderColor: '#009387',
    borderWidth: 1,
    borderStyle:"dashed",
    borderRadius:10,
    backgroundColor:'#fff'

  },
  icons: {  
      flexDirection:'row',justifyContent:'space-between',marginTop:10
  },

});