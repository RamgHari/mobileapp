import * as React from 'react';
import { Button,Image, View, Text, StyleSheet,Dimensions,TouchableOpacity,TextInput ,StatusBar,Alert,ActivityIndicator} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {LinearGradient} from 'expo-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather'
import * as Facebook from "expo-facebook";
import * as Google from 'expo-google-app-auth';
import styles from './style.js';
import Constants from 'expo-constants';
import { useBackHandler } from "@react-native-community/hooks"
import SocialButton from '../../../components/SocialButton';
import {onUserLogin,onUserSocialLogin,setLoading} from '../../../redux'
import {useDispatch} from 'react-redux';
import firebase from 'firebase';
import firebaseConfig from './config';
if (!firebase.apps.length) {
  firebase.initializeApp({firebaseConfig});
}else {
  firebase.app(); // if already initialized, use that one
}
const  SigninScreen=(props) =>{
  const isInExpo = Constants.appOwnership === 'expo';
  const dispatch = useDispatch()
    const [data, setData] = React.useState({
        username: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser:true,
        isValidPassword:true
    });

    const IOS_CLIENT_ID =
  "944656838741-9p2bkvkhaeqimq52v42f22h9tj2hl9fu.apps.googleusercontent.com";
const ANDROID_CLIENT_ID =
  "402552765613-0ulrau1oc1gsm95j264geues9qtn11su.apps.googleusercontent.com";

  const datas=isInExpo?
  {iosClientId: '33860000961-un4ghka1k2sepcnj4gvqeoge4bndf25k.apps.googleusercontent.com',
  androidClientId: ANDROID_CLIENT_ID,scopes: ["profile", "email"]}:
  {iosStandaloneAppClientId: '33860000961-un4ghka1k2sepcnj4gvqeoge4bndf25k.apps.googleusercontent.com',
  androidStandaloneAppClientId: ANDROID_CLIENT_ID,scopes: ["profile", "email"]}

    const textInputChange=(val)=>{
        if( val.trim().length >= 4 ) {
            setData({
                ...data,
                username: val,
                check_textInputChange: true,
                isValidUser:true
            });
        } else {
            setData({
                ...data,
                username: val,
                check_textInputChange: false,
                isValidUser: false,
            });
        }
    }
    const handlePasswordChange=(val)=>{
        if( val.trim().length >= 8 ) {
        setData({
            ...data,
            password:val,
            isValidPassword:true
        })
        }else{
            setData({
                ...data,
                password:val,
                isValidPassword:false
            })
        }
    }
    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }


    const handleValidUser=(val)=>{
        if(val.trim().length>=4){
            setData({
                ...data,
                isValidUser:true
            })
        }else{
            setData({
                ...data,
                isValidUser:false
            })
        }
    }

    const FBlogIn=async()=> {
        try {
          await Facebook.initializeAsync({appId:'151329231708133'});
          const {
            type,
            token,
            expires,
            permissions,
            declinedPermissions,
          } = await Facebook.logInWithReadPermissionsAsync({
            permissions: ["public_profile", "email"],
          });
          if (type === "success") {
            // Get the user's name using Facebook's Graph API
            const response = await fetch(
              `https://graph.facebook.com/me?access_token=${token}`
            );
            let responseJSON = await response.json();
            dispatch(setLoading())
            FBLoginSignup(responseJSON);


          } else {
            // type === 'cancel'
          }
        } catch ({ message }) {
          alert(`Facebook Login Error: ${message}`);
        }
      }


      const FBLoginSignup = (fbdata) => {
        let datenow = new Date();
        fetch(
          "https://www.allskills.in/includes/ajaxFromApp.php?request=fbloginSignupAppUser",
          {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              fb_id: fbdata.id,
              fb_email: fbdata.id,
              fb_name: fbdata.name,
              time: +datenow,
            }),
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.status == "success") {
                dispatch(onUserSocialLogin(responseJson))
            } else {
              alert("Login failed");
            }
          });
      };


      const GooglelogIn = async () => {
        try {
            const result = await Google.logInAsync(
                datas,

              );
              if (result.type === "success") {
                dispatch(setLoading())
                GOOGLELoginSignup(result.user);
              } else {
                return { cancelled: true };
              }
            } catch (e) {
              return { error: true };
            }
      };

const  GOOGLELoginSignup = (googleData) => {
   let datenow = new Date();
    fetch(
      "https://www.allskills.in/includes/ajaxFromApp.php?request=GOOGLEloginSignupAppUser",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          google_id: googleData.id,
          google_email: googleData.email,
          google_name: googleData.name,
          photoUrl: googleData.photoUrl,
          time: +datenow,
        }),
      }
    )
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status == "success") {
            dispatch(onUserSocialLogin(responseJson))
        } else {
          alert(responseJson.message);
        }
      });
  };
  
  // Callback function for back action
  const backActionHandler = () => {
    props.navigation.push('Signin')
    return true;
  };

// hook which handles event listeners under the hood
  useBackHandler(backActionHandler)

  return (
    <View style={styles.container}>
        <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <View animation="fadeInUpBig" style={styles.footer}>
        <View style={styles.imgcontainer}>
        <Image
        source={require('../../../assets/images/cicon.png')}
        style={styles.logo}
      />
      </View>
            <Text style={styles.text_footer}></Text>
            <View style={styles.action}>
                <FontAwesome
                    name="user-o"
                    color="#05375a"
                    size={20}
                />
                <TextInput
                    placeholder="Enter Your Username"
                    style={styles.textInput}
                    autoCapitalize = 'none'
                    onChangeText={(val) => textInputChange(val)}
                    value={data.username}
                    onEndEditing={(e)=>handleValidUser(e.nativeEvent.text)}
                    />
                {data.check_textInputChange ?
                <Animatable.View
                    animation="bounceIn">
                    <Feather
                        name="check-circle"
                        color="green"
                        size={20}
                    />
                </Animatable.View>:null}
            </View>
            { data.isValidUser ? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>Username must be 4 characters long.</Text>
            </Animatable.View>
            }
            <Text style={[styles.text_footer,{marginTop:35}]}></Text>
            <View style={styles.action}>
                <FontAwesome
                    name="lock"
                    color="#05375a"
                    size={20}
                />
                <TextInput
                    placeholder="Enter Your Password"
                    secureTextEntry={data.secureTextEntry?true:false}
                    style={styles.textInput}
                    value={data.password}
                    onChangeText={(val) => handlePasswordChange(val)}
                    />
                <TouchableOpacity onPress={updateSecureTextEntry}>
                    <Feather
                        name={data.secureTextEntry?'eye-off':'eye'}
                        color="gray"
                        size={20}
                        />
                </TouchableOpacity>
            </View>
            { data.isValidPassword ? null :
            <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>Password must be 8 characters long.</Text>
            </Animatable.View>
            }
            <View style={styles.button}>
                <TouchableOpacity
                    style={styles.signIn}

                    onPress={() => {dispatch(onUserLogin(data.username,data.password))}} >
                <LinearGradient
                    colors={['#08d4c4', '#01ab9d']}
                    style={styles.signIn}>
                    <Text style={[styles.textSign, {color:'#fff'}]}>Sign In</Text>
                </LinearGradient>
                </TouchableOpacity>

          <SocialButton
            buttonTitle="Sign In with Facebook"
            btnType="facebook"
            color="#4867aa"
            backgroundColor="#e6eaf4"
            onPress={() => {FBlogIn()}}
          />

          <SocialButton
            buttonTitle="Sign In with Google"
            btnType="google"
            color="#de4d41"
            backgroundColor="#f5e7ea"
            onPress={() => GooglelogIn()}
          />


                <TouchableOpacity
                    onPress={() => props.navigation.navigate('Signup')}
                    style={[styles.signIn, {
                        borderColor: '#009387',
                        borderWidth: 1,
                        marginTop: 30
                    }]}>
                    <Text style={[styles.textSign, {color: '#009387'}]}>Sign Up</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
  );
}
// const mapStateToProps=(state)=>({
//   userReducer:state.userReducer
// })
// const SigninScreen=connect(mapStateToProps,{onUserLogin})(_LoginScreen)
export default SigninScreen;
