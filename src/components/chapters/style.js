import { StyleSheet } from 'react-native';
import { colors } from "../../assets/color/style";
export default StyleSheet.create({
    descText: {
        flexDirection: "row",
        paddingHorizontal: 8,
        paddingBottom: 5,
        paddingVertical: 5,
      },
      name:{
        paddingHorizontal: 10,
        fontSize: 17,
        color: colors.textGray,
      }
});
