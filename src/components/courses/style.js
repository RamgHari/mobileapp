import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    touch:{
        flexDirection:"row",
        backgroundColor:"#009387",
        padding:20,
        marginHorizontal:20,
        borderRadius:20,
        alignItems:"center",
        marginTop:10
    },
    courseName:{
        color:"#fff",
        fontFamily:"Bold",
        fontSize:13,
        paddingHorizontal:20,
        width:170
    },
    space:{
        color:"#345c74",
        fontFamily:"Medium",
        fontSize:13,
        width:100
    }
});
