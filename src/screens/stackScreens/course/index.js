import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  TextInput,
} from "react-native";
import { Feather } from "@expo/vector-icons";
import { colors } from "../../../assets/color/style";
import { useSelector} from 'react-redux';
import axios from 'axios';
const CoursesScreen= (props) => {
  const users=useSelector(state => state.userReducer.user)
  const subjects=props.route.params.item;
  const categoryId={categoryId:subjects.id}
  const [courses,setCourses]=React.useState([])
  const [query, setQuery] = useState('');
  const [fullData, setFullData] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);
  const [error, setError] = React.useState(null);
  const starColors = ["#e3ab53", "#e3ab53", "#e3ab53", "#e3ab53", "#8b6f43"];
  React.useEffect(() => {
    setIsLoading(true);
    axios.post(`https://www.allskills.in/includes/ajaxFromApp.php?request=getCourseListByCategories`,categoryId)
    .then((res) =>res.data)
  .then(
    (result) => {

      setIsLoading(false);
      setCourses(result)
      setFullData(result);
    },
    (error) => {
      setIsLoading(false);
      setError(err);
    } 
  );

  }, [])
  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color="#5500dc" />
      </View>
    );
  }

  if (error) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text style={{ fontSize: 18}}>
          Error fetching data... Check your network connection!
        </Text>
      </View>
    );
  }
  function renderHeader() {
    return (
      <View style={{ paddingVertical: 20, backgroundColor: '#009387',flexDirection:'row',alignItems:'center'}}>
      <TouchableOpacity style={{ marginLeft:40}}
          onPress={() => props.navigation.goBack()}
        >
          <Feather name="arrow-left" size={22} color="white" />
        </TouchableOpacity>
    <TextInput
      style={styles.searchBox}
      autoCorrect={false}
      autoCapitalize="none"
      value={query}
      onChangeText={queryText => handleSearch(queryText)}
      placeholder="Search"
    ></TextInput>
    {/* <Feather
      name="search"
      size={22}
      color="#666"
      style={{ position: "absolute", top: 50, right: 60, opacity: 0.6 }}
    /> */}
  </View>
    );
  }

  const handleSearch = text => {
    const formattedQuery = text.toLowerCase();
    const filteredData = fullData.filter((courses) => {
      return courses.name.toLowerCase().indexOf(formattedQuery.toLowerCase())>=0
    });
    setCourses(filteredData);
    setQuery(text);
  };
  
  
  return (
    <View style={styles.container}>
      <FlatList
        data={courses}
        ListHeaderComponent={renderHeader}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity  onPress={() => props.navigation.navigate("EnrollCourse",{item,users})}>
              <View style={styles.cateContainer}>
                <Image
                  source={{uri:`https://www.allskills.in/uploads/course/${item.image}`}}
                  style={{
                    width: 60,
                    height: 60,
                    marginVertical: 10,
                    marginLeft: 5,
                  }}
                />
                <View
                  style={{ paddingLeft: 20, marginVertical: 10, width: "75%" }}
                >
                  <View>
                    <Text style={{ fontSize: 17 }}>{item.name}</Text>
                    <Text
                      style={{
                        fontSize: 15,
                        color: colors.textGray,
                        paddingVertical: 2,
                      }}
                    >
                      Course by {item.userDetails.first_name}
                    </Text>
                    <Text style={{ textAlign: "left", color: colors.textGray }}>
                    {item.description.length < 100
                ? `${item.description}`
                : `${item.description.substring(0, 97)}...`}
                    </Text>
                   <View style={{ flexDirection: "row", paddingVertical: 5 }}>
                      {starColors.map((color, index) => {
                        return (
                          <Feather
                            name="star"
                            size={14}
                            color={color}
                            key={index}
                            style={{ marginRight: 4 }}
                          />
                        );
                      })}
                       {/* <Text
                        style={{
                          fontSize: 14,
                          fontWeight: "bold",
                          paddingLeft: 20,
                          color: colors.textGray,
                        }}
                      >
                        4 (55,400)
                      </Text>*/}
                    </View> 
                    {/* <View style={{ flexDirection: "row" }}>
                      <Text style={{}}>MMK 19990</Text>
                      <Text
                        style={{
                          color: colors.textGray,
                          paddingLeft: 20,
                          textDecorationLine: "line-through",
                        }}
                      >
                        MMK 29990
                      </Text>
                    </View> */}
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  searchContainer: {
    paddingTop: 100,
    paddingLeft: 16,
  },
  setTitle: {
    fontSize: 38,
    fontWeight: "bold",
    color: colors.defaultRed,
  },
  setTitleText: {
    fontSize: 16,
    fontWeight: "normal",
    color: colors.defaultRed,
  },

  searchBox: {
    marginTop: 16,
    backgroundColor: "#fff",
    paddingLeft: 24,
    padding: 12,
    borderTopRightRadius: 40,
    borderBottomRightRadius: 40,
    width: "70%",
    marginLeft:40
  },
  cateContainer: {
    backgroundColor: "white",
    flexDirection: "row",
    borderBottomColor: colors.textGray,
    borderBottomWidth: 0.5,
    marginHorizontal: 10,
  },
});

export default CoursesScreen;
