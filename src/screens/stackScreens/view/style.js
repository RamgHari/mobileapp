import { StyleSheet } from 'react-native';

export default StyleSheet.create({
bgimg:{
    width:"100%",
    height:"100%"
},
bgimage:{
    height:200,
    width:300,
    alignSelf:"center",
    marginTop:20
},
head:{
    color:"#FFF",
    fontFamily:"Bold",
    fontSize:35,
    width:200,
    alignSelf:"center",
    textAlign:"center"
},
handlestyle:{
    marginTop:30,
    backgroundColor:"#e9e9e9",
    width:80
},
modalstyle:{
    borderTopLeftRadius:30,
    borderTopRightRadius:30
},
container:{
    // flexDirection:"row",
     marginHorizontal:30,
     marginTop:30
 },
 img:{
    height:100,
    width:200,
    alignSelf:"center",
    
},
head:{
    color:"#333",
    fontFamily:"Bold",
    fontSize:25,
    width:200,
    alignSelf:"center",
    textAlign:"center"
},
description:{
    color:"#333",
    fontSize:25,
    width:300,
    alignSelf:"center",
    textAlign:"center"
},
});