import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome';


const  HomeScreen=({ navigation }) =>{
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() => navigation.navigate('Details')}
      />
    </View>
  );
}

const DetailScreen=({ navigation })=> {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
      <Button
        title="Go to Details... again"
        onPress={() => navigation.push('Details')}
      />
    </View>
  );
}

const HomeStack = createStackNavigator();
const DetailStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const HomeStackScreen=({ navigation })=> {
  return (
   <HomeStack.Navigator screenOptions={{
     headerStyle:{
      backgroundColor:'red'
     },
     headerTintColor:'#fff',
     headerTitleStyle: { 
      textAlign:"center", 
      flex:1 
  }
   }}>
        <HomeStack.Screen name="Home" component={HomeScreen} 
        options={{
          title:'HomePages',
          headerLeft:()=>{
            <Icon.Button
            name="facebook"
            size={25} backgroundColor='#900' onPress={()=>{navigation.openDrawer()}}></Icon.Button>
  
          }
        }} />
      </HomeStack.Navigator>
  );
}
const DetailStackScreen=({ navigation }) =>{
  return (
   <DetailStack.Navigator screenOptions={{
    headerStyle:{
     backgroundColor:'red'
    },
    headerTintColor:'#fff',
    headerTitleStyle: { 
     textAlign:"center", 
     flex:1 
 }
  }}>
        <HomeStack.Screen name="Details" component={DetailScreen} 
         options={{
          title:'Detail Page',
          headerLeft:()=>{
            <Icon.Button
            name="facebook"
            size={25} backgroundColor='#900' onPress={()=>{navigation.openDrawer()}}></Icon.Button>
  
          }
        }} />
    </DetailStack.Navigator>
  );
}



function App() {
  return (
    <NavigationContainer>
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Home" component={HomeStackScreen} />
      <Drawer.Screen name="Details" component={DetailStackScreen} />
    </Drawer.Navigator>
  </NavigationContainer>
  
   

  );
}

export default App;
