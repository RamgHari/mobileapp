import React, { useEffect } from 'react'
import {View,Text} from 'react-native'
import styles from './style.js';
import { colors } from "../../assets/color/style";
import Lessons from '../lessons';

const Chapters=(props)=>{
  
        const {course,name,lessonDetails} = props.lessons
        
        return(
            <View style={styles.descText}>
                            <Text
                              style={styles.name}>
                              Chapter {props.index+1}
                            </Text>
                            <View style={{ paddingLeft: 10 }}>
                              <Text style={{ fontSize: 17, color: colors.textGray }}>{name}</Text>
                              {lessonDetails.map((item,index)=>{
                                 return(<Lessons key={index} index={index} count={index+1} lessonDetails={item}/>)
                        })
                    }
                            </View>
            </View>
        )
    }

export default Chapters