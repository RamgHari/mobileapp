import React from 'react'
import {Text,TouchableOpacity, View} from 'react-native'
import styles from './style.js';
const  Subject=(props)=>{

        const {id,name} = props.subjects
        const onPress=props.onPress
        return(
            <TouchableOpacity
             onPress={onPress}
                style={styles.container}>
                    <View>
                         <Text style={styles.head}>{name}</Text>
                    </View>
            </TouchableOpacity>
        )
    
}
export default Subject;