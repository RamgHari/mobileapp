import React from "react";
import { View, TouchableOpacity, Text } from "react-native";
import Modal from "react-native-modal";
import {LinearGradient} from 'expo-linear-gradient';
import TextArea from "@freakycoder/react-native-text-area";

const Comments = props => {
//   const { starRating, onStarRatingPress } = props;

const submitComments=(cmt)=>{
    
    props.handlesubmitComment(cmt)
}
const [cmt,setCmt]=React.useState()
  return (
    <Modal isVisible={true}>
      <View
        style={{
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 7
          },
          shadowOpacity: 0.43,
          shadowRadius: 9.51,
          elevation: 10,
          height: 325,
          width: "90%",
          borderRadius: 16,
          alignSelf: "center",
          justifyContent: "center",
          backgroundColor: "transparent"
        }}
      >
        <View
          style={{
            height: "100%",
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "space-around"
          }}
        >
          <Text style={{ color: "white", fontSize: 16 }}>
            Comments?
          </Text>
          <TextArea
            maxCharLimit={50}
            placeholderTextColor="black"
            exceedCharCountColor="red"
            value={cmt}
            placeholder={"Write your review..."}
            style={{ height: 150, borderRadius: 16 }}
            onChangeText={setCmt}
          />
          <TouchableOpacity
            style={{
              height: 50,
              width: "95%",
              borderRadius: 16,
              backgroundColor: "white"
            }}
            onPress={()=>submitComments(cmt)}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={{
                height: 50,
                width: "100%",
                borderRadius: 16,
                alignContent: "center",
                justifyContent: "center"
              }}
              colors={["#5f2c82", "#49a09d"]}
            >
              <Text
                style={{ color: "white", fontSize: 16, textAlign: "center" }}
              >
                Submit
              </Text>
              

              
            </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity
            style={{
              height: 50,
              width: "95%",
              borderRadius: 16,
              backgroundColor: "white"
            }}
            onPress={props.closeModal}
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={{
                height: 50,
                width: "100%",
                borderRadius: 16,
                alignContent: "center",
                justifyContent: "center",
              }}
              colors={["#5f2c82", "#49a09d"]}
            >
              <Text
                style={{ color: "white", fontSize: 16, textAlign: "center" }}
              >
                Close
              </Text>
              

              
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};



export default Comments;