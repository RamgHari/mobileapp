import React, { useState }  from 'react';
import { View, ScrollView, Text, TextInput, TouchableOpacity,Image } from 'react-native';
import { Container, Spinner } from 'native-base';
import { Ionicons } from '@expo/vector-icons'; 
import {Avatar} from 'react-native-paper';
import { EvilIcons } from '@expo/vector-icons'; 


const CommentView=(props)=> {
  const [formId,setFormId]=useState(props.route.params.item.id)
  const [text, setText] =  useState('')
  const [comments,setComments]=useState(props.route.params.item.feedComments)
  // console.log("comments",comments)

  function send() {
    if (text !== '') {
    //   sendComment(text, movie.title);
    //   setText('');
    }
  }

  return (
    comments.loading?(
      <Spinner />
    ) : (
      <Container>
        <ScrollView>
          {comments.map((comment, index) => {
            return(
              

                <View style={styles.commentcontainer} key={comment.id}>
                        
                      <View style={styles.imgInfo}>
                               
                      <Avatar.Image 
                      source={{uri:comment.userDetails.image}}
                      size={30}/>
                     
                     
                      <View style={styles.userInfo}>
                              <Text style={styles.comment}> {comment.userDetails.first_name}</Text>
                              <Text> {comment.comment}</Text>
                              
                      </View>
                      </View>
                     </View>
             )
          })} 
        </ScrollView>
        {/* <View style={styles.inputContainer}>
          <TextInput
            placeholder="Type a message..."
            style={styles.textInput}
            onChangeText={text => setText(text)}
            value={text}
          />
          <TouchableOpacity style={styles.sendButton} onPress={() => send()}>
            <Ionicons name="md-send" size={24} color="black" />
          </TouchableOpacity>
        </View> */}
      </Container>
    )
  );
}

export default CommentView;

const styles = {
  textInput: {
    height: 40,
    flex: 0.9,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,
    padding: 5
  },
  inputContainer: {
    margin: 10,
    height: 40,
    bottom: 0,
    flexDirection: 'row'
  },
  sendButton: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  userInfo:{
    marginLeft:10,
    flexDirection:"column",
    justifyContent:'center'
  },
  imgInfo:{
    flexDirection: 'row',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,
    padding: 5
   
  },
  comment:{
    fontSize: 14,
    color: '#000',
    fontFamily:'Regular'
  },
  commentcontainer:{
  margin:10,
  paddingTop:10,
  paddingRight:10,
  paddingLeft:10,
  borderRadius:10,
  backgroundColor:'#fff'
  },
}