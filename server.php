<?php header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true ");
header("Access-Control-Allow-Methods: OPTIONS, GET, POST");
header("Access-Control-Allow-Headers: Content-Type, Depth, User-Agent, X-File-Size,
    X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
error_reporting(0);
if(!isset($_SESSION)) session_start();

error_reporting(E_ALL);
ini_set('display_errors', '1');


require_once 'db_config.php';

include 'classes/Apps.php';
include 'classes/Users.php';
include 'classes/Course.php';
include 'classes/Page.php';
include 'classes/Feed.php';
include 'classes/Email.php';


$obj_Apps = new Apps($con);
$settings = $obj_Apps->getSettings();
$APP_PATH = $obj_Apps->getAppPath();
$obj_Users = new Users($con,$APP_PATH);
$obj_Course = new Course($con,$APP_PATH);
$obj_Page = new Page($con,$APP_PATH);
$obj_Feed = new Feed($con,$APP_PATH);


function getGoogleGeoDetails($lati,$long){

  $geocode=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lati.','.$long.'&sensor=false&key=AIzaSyBqNWRXRRA-yYPKda3_Ispn6zzsDQBYwL8');

  $output= json_decode($geocode);
  $result = array();
  for($j=0;$j<count($output->results[0]->address_components);$j++){
          $result[$output->results[0]->address_components[$j]->types[0]] = $output->results[0]->address_components[$j]->long_name;
//                echo '<b>'.$output->results[0]->address_components[$j]->types[0].': </b>  '.$output->results[0]->address_components[$j]->long_name.'<br/>';
          }
  return $result;
}

if($_REQUEST['request'] == "getLocationDetails"){
  $getdata = json_decode(file_get_contents('php://input'), true);
  if(isset($getdata['location']) && $getdata['location']["coords"]["latitude"] != '' && $getdata['location']["coords"]["longitude"] != ''){
      $result = getGoogleGeoDetails($getdata['location']["coords"]["latitude"],$getdata['location']["coords"]["longitude"]);
  }else{
      $result = [];
  }

      // $response = [
      //   "status" => $getdata['location']["coords"]["latitude"],
      //     "user" => $getdata['location']["coords"]["longitude"]
      // ];

      // echo json_encode($response);

  echo json_encode($result);

}

if(@$_REQUEST['request'] == "getChaptersAndLessonsByCourse")
{
  $status = "failed";
  $data['courseId'] = 28;
  $getData= $obj_Course->getChaptersAndLessonsByCourse($data['courseId']);
  if($getData){
    $status = "success";
  }
  $response = [
    "status" => $status,
    "data" => $getData,
  ];
  echo json_encode($response);

}


if(@$_REQUEST['request'] == "fbloginSignupAppUser") //Request from react native app
{
    $user = "";
    $message = "";
    $status = "failed";

    $data = json_decode(file_get_contents('php://input'), true);

    // $data['fb_id'] =  2744345492502020;
    // $data['fb_email'] =  2744345492502020;
    // $data['fb_name'] =  'Ramesh Aucto';
    // $data['time'] =  '12345';
    $checkSocialUser = $obj_Users->checkSocialUser($data['fb_id']);
    if($checkSocialUser){
      $status = "success";
      $user = $checkSocialUser;
      $user['userProfileImage'] = 'https://graph.facebook.com/'.$user['social_id'].'/picture?type=square';
    }else{

        // Remote image URL
        $url = 'https://graph.facebook.com/'.$data['fb_id'].'/picture?type=square';

        // Image path
        $data['image'] = 'fb_'.$data['fb_id'].'.jpeg';
        $_REQUEST['image_path'] = '../uploads/user/' . $data['image'];
        // Save image
        file_put_contents($_REQUEST['image_path'], file_get_contents($url));

        $data['email'] = $data['fb_email'];
        $data['social_id'] = $data['fb_id'];
        $data['fname'] = $data['fb_name'];
        $data['lname'] = "";
        $data['social'] = "facebook";
        $data['role'] = 2;
        $data['datetime'] = $data['time'];
        $userData = $obj_Users->insertSocialUser($data);
        if($userData){
          $status = "success";
          $user = $userData;
          $user['userProfileImage'] = $url;
        }
    }

    $response = [
      "status" => $status,
      "user" => $user,
      "message" => $message,
      "data"=>$data
    ];

    echo json_encode($response);
}

if(@$_REQUEST['request'] == "GOOGLEloginSignupAppUser") //Request from react native app
{
    $user = "";
    $message = "";
    $status = "failed";

    $data = json_decode(file_get_contents('php://input'), true);

    // $data['google_id'] =  114529304841655174598;
    // $data['google_email'] =  'rameshdreamweb@gmail.com';
    // $data['google_name'] =  'Ramesh Ngl';
    // $data['time'] =  '12345';
    // $data['photoUrl'] = 'https://lh5.googleusercontent.com/-BCPHkgf7HwI/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucmBuJ-Imlr-pxuNhXW7LehZycGeSQ/photo.jpg';

    $checkSocialUser = $obj_Users->checkSocialUser($data['google_id']);
    $checkEmailExist = $obj_Users->checkEmailExist($data['google_email']);


    if($checkSocialUser || $checkEmailExist > 0){
      $status = "success";
      $user = $checkSocialUser;
      $user['userProfileImage'] = $data['photoUrl'];
    }else{

        // Remote image URL
        $url = $data['photoUrl'];

        // Image path
        $data['image'] = "google_".$data['google_id'].'.jpg';
        $_REQUEST['image_path'] = '../uploads/user/'. $data['image'];
        // Save image
        file_put_contents($_REQUEST['image_path'], file_get_contents($url));

        $data['email'] = $data['google_email'];
        $data['social_id'] = $data['google_id'];
        $data['fname'] = $data['google_name'];
        $data['lname'] = "";
        $data['social'] = "google";
        $data['role'] = 2;
        $data['datetime'] = $data['time'];
        $userData = $obj_Users->insertSocialUser($data);
        if($userData){
          $status = "success";
          $user = $userData;
          $user['userProfileImage'] = $url;
        }
    }

    $response = [
      "status" => $status,
      "user" => $user,
      "message" => $message,
      "data"=>$data
    ];

    echo json_encode($response);
}

if(@$_REQUEST['request'] == "getAllTrainingInstitutes") //Request from react native app
{
  $select = mysqli_query($con,"select * from `training_institutes`") ;
  $row = mysqli_fetch_all($select,MYSQLI_ASSOC);
  $response = [
    "data" => $row
  ];

  echo json_encode($response);
}
if(@$_REQUEST['request'] == "getAllSkillCenters") //Request from react native app
{
  $select = mysqli_query($con,"select * from `skill_centers`") ;
  $row = mysqli_fetch_all($select,MYSQLI_ASSOC);
  $response = [
    "data" => $row
  ];

  echo json_encode($response);
}
if(@$_REQUEST['request'] == "loginUser")
{
    $data = json_decode(file_get_contents('php://input'), true);
    // $data["signinUsername"] = "admin";
    // $data["signinPassword"] = "goodluck";
     $userData = $obj_Users->login($data["signinUsername"],md5($data["signinPassword"]));
    if($userData > 0){

      $userProfileDetails = $obj_Users->getUserProfileDetails($userData['id']);
      if($userProfileDetails){
        $userData = array_merge($userData,$userProfileDetails);
      }

      if( $userData['social_id'] != '' && filter_var($userData['image'], FILTER_VALIDATE_URL) === true){
        $userData['userProfileImage'] = $userData['image'];
      }else if($userData['image'] != ''){
        $userData['userProfileImage'] = $APP_PATH.'/uploads/user/'.$userData['image'];
      }else{
        $userData['userProfileImage'] = $APP_PATH.'/uploads/user/default_profile.png';
      }
      $status = "success";
    }else{
        $status = "failed";
    }

    $response = [
      "status" => $status,
      "user" => $userData
    ];

   echo json_encode($response);

}

if(@$_REQUEST['request'] == "registerUser"){

    $user = "";
    $message = "";
    $status = "failed";
    $data = json_decode(file_get_contents('php://input'), true);
    $checkEmailExist = $obj_Users->checkEmailExist($data['signupEmail']);
    //$checkUsernameExist = $obj_Users->checkUsernameExist($_REQUEST['signupUsername']);
    // else if($checkUsernameExist > 0){
    //     $message = "Username exist.please try different";
    // }

    if($checkEmailExist > 0){
        $message = "Email already registered by another user";
    }else{
      $data['signupUsername'] = $data['signupEmail'];
      $data['signupFirstName'] = "New";
      $data['signupLastName'] = "User";


      $userData = $obj_Users->insertAppUser($data);
      $status = "success";
      $user = $userData;
      // $_SESSION['loggedIn']= true;
      // $_SESSION['userData']= json_encode($user);
      // $_SESSION['username'] = $user["username"];
      // $_SESSION['password'] = $user["password"];

    }

    $response = [
      "status" => $status,
      "message" => $message
    ];

   echo json_encode($response);

}

if(@$_REQUEST['request'] == "doSocialLogin"){

    $user = "";
    $message = "";
    $status = "failed";
    $folderName = "";

    $checkSocialUser = $obj_Users->checkSocialUser($_REQUEST['social_id']);

    if($checkSocialUser){
      $status = "success";
      $user = $checkSocialUser;
      $_SESSION['loggedIn']= true;
      $_SESSION['userData']= json_encode($user);
      $userRoleDetails = $obj_Apps->getRoleById($user["role"]);
      $folderName = $userRoleDetails['folder'];
    }else{

      if($_REQUEST['social']=="facebook"){
        // Remote image URL
        $url = 'https://graph.facebook.com/'.$_REQUEST['social_id'].'/picture?type=square';

       // Image path
       $_REQUEST['image'] = $_REQUEST['social_id'].'.jpeg';
       $_REQUEST['image_path'] = '../uploads/user/fb_' . $_REQUEST['image'];
       // Save image
       file_put_contents($_REQUEST['image_path'], file_get_contents($url));

      }
      $userData = $obj_Users->insertSocialUser($_REQUEST);
      if($userData){
        $status = "success";
        $user = $userData;
        $_SESSION['loggedIn']= true;
        $_SESSION['userData']= json_encode($user);
        $userRoleDetails = $obj_Apps->getRoleById($user["role"]);
        $folderName = $userRoleDetails['folder'];
      }

    }

    $response = [
      "status" => $status,
      "message" => $message,
      "folder" => $folderName
    ];

   echo json_encode($response);

}

if(@$_REQUEST['request'] == "getAllCourses") //Request from react native app
{

  $status = "failed";
  $getCourses = $obj_Course->getAllCoursesList();
  $getAvailableCourseSubjectTypes = $obj_Course->getAvailableCourseSubjectTypes();
  if($getCourses){
    $status = "success";
  }
  $response = [
    "status" => $status,
    "data" => $getCourses,
    "subjectTypes" => $getAvailableCourseSubjectTypes
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "getChaptersAndLessons") //Request from react native app
{
  $status = "failed";
  $data = json_decode(file_get_contents('php://input'), true);
  //$data['courseId'] = 28;
  //$data['userId'] = 6;
  $getData= $obj_Course->getChaptersAndLessonsByCourseForMobileApp($data);
  if($getData){
    $status = "success";
  }
  $response = [
    "status" => $status,
    "data" => $getData,
  ];
  echo json_encode($response);
}


if(@$_REQUEST['request'] == "getAllUserCourses") //Request from react native app
{

  $status = "failed";

  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $getCourses = $obj_Course->getAllUserCourses($userData->id);
    if($getCourses){
      $status = "success";
    }
  }



  $response = [
    "status" => $status,
    "data" => $getCourses
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "addCourse") //Request from react native app
{
  $status = "failed";
  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $_REQUEST['name'] = addslashes ($_REQUEST['name']);
    $_REQUEST['description'] = addslashes ($_REQUEST['description']);
    $_REQUEST['user_id'] = $userData->id;
    $insert = $obj_Course->insertCourse($_REQUEST);
    if($insert){
      $status = "success";
    }
  }

  $response = [
    "status" => $status,
    "data" => $_REQUEST
  ];
   echo json_encode($response);

}


if(@$_REQUEST['request'] == "editCourse") //Request from react native app
{
  $status = "failed";
  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $_REQUEST['name'] = addslashes ($_REQUEST['name']);
    $_REQUEST['description'] = addslashes ($_REQUEST['description']);
    $_REQUEST['user_id'] = $userData->id;
    $update = $obj_Course->updateCourse($_REQUEST);
    if($update){
      $status = "success";
    }
  }
  $response = [
    "status" => $status,
    "data" => $_REQUEST
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "addLesson") //Request from react native app
{
  $status = "failed";
  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $_REQUEST['name'] = addslashes ($_REQUEST['name']);
    $_REQUEST['user_id'] = $userData->id;

    if(@$_FILES['image']['name'] != ''){
        $_REQUEST['image'] = time()."lesson".$_FILES['image']['name'];
        move_uploaded_file($_FILES['image']['tmp_name'], '../uploads/course/lesson/' . $_REQUEST['image']);
    }
    $insert = $obj_Course->insertLesson($_REQUEST);
    if($insert){
      $status = "success";
    }
  }

  $response = [
    "status" => $status,
    "data" => $_REQUEST
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "editLesson") //Request from react native app
{
  $status = "failed";
  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $_REQUEST['name'] = addslashes ($_REQUEST['name']);
    $_REQUEST['user_id'] = $userData->id;

    if(@$_FILES['image']['name'] != ''){
        $_REQUEST['image'] = time()."lesson".$_FILES['image']['name'];
        move_uploaded_file($_FILES['image']['tmp_name'], '../uploads/course/lesson/' . $_REQUEST['image']);
    }else{
        $_REQUEST['image'] = $_REQUEST['image_value'];
    }
    $update = $obj_Course->updateLesson($_REQUEST);
    if($update){
      $status = "success";
    }
  }

  $response = [
    "status" => $status,
    "data" => $_REQUEST
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "getAllLessons") //Request from react native app
{

  $status = "failed";
  $getAllLessons= $obj_Course->getAllLessons();
  if($getAllLessons){
    $status = "success";
  }
  $response = [
    "status" => $status,
    "data" => $getAllLessons
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "addChapter") //Request from react native app
{
  $status = "failed";
  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $_REQUEST['name'] = addslashes ($_REQUEST['name']);
    $_REQUEST['user_id'] = $userData->id;

    $insert = $obj_Course->insertChapter($_REQUEST);
    if($insert){
      $status = "success";
    }
  }

  $response = [
    "status" => $status,
    "data" => $insert
  ];
   echo json_encode($response);

}


if(@$_REQUEST['request'] == "editChapter") //Request from react native app
{
  $status = "failed";
  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $_REQUEST['name'] = addslashes ($_REQUEST['name']);
    $_REQUEST['user_id'] = $userData->id;

    $update = $obj_Course->updateChapter($_REQUEST);
    if($update){
      $status = "success";
    }
  }

  $response = [
    "status" => $status,
    "data" => $_REQUEST
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "getAllChapters") //Request from react native app
{

  $status = "failed";
  $getAllChapters= $obj_Course->getAllChapters();
  if($getAllChapters){
    $status = "success";
  }
  $response = [
    "status" => $status,
    "data" => $getAllChapters
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "doCoursePublishDraft") //Request from react native app
{

  $status = "failed";

  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $_REQUEST['user_id'] = $userData->id;
    $updateCourse= $obj_Course->updateCoursePublish($_REQUEST);
    if($updateCourse){
      $status = "success";
    }
  }


  $response = [
    "status" => $status
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "addPage") //Request from react native app
{
  $status = "failed";
  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $_REQUEST['name'] = addslashes($_REQUEST['name']);
    $_REQUEST['address'] = addslashes($_REQUEST['address']);
    $_REQUEST['user_id'] = $userData->id;

    if(@$_FILES['image']['name'] != ''){
        $_REQUEST['image'] = time()."page".$_FILES['image']['name'];
        move_uploaded_file($_FILES['image']['tmp_name'], '../uploads/page/' . $_REQUEST['image']);
    }else{
        $_REQUEST['image'] = "";
    }
    $insert = $obj_Page->insert($_REQUEST);
    if($insert){
      $status = "success";
    }
  }
  $response = [
    "status" => $status,
    "data" => $_REQUEST
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "postFeedFromApp"){


  $status = "failed";
  if($_REQUEST['type'] == 'picture' && @$_FILES['value']['name'] != ''){
      $_REQUEST['feedImg'] = time()."feed".$_FILES['value']['name'];
      $_REQUEST['feedType'] = "picture";
  }else{
      $_REQUEST['feedImg'] = '';
      $_REQUEST['feedType'] = '';
  }
  $userId = $_REQUEST['userId'];
  $FeedTime = $_REQUEST['datetime'];
  $feedData = array(
    'user_id' => $userId,
    'message' => $_REQUEST['feedContent'],
    'type' => $_REQUEST['feedType'],
    'value' => $_REQUEST['feedImg'],
    'time' => $FeedTime
  );
  $insertFeed = $obj_Feed->insert("messages",$feedData);
  if($insertFeed){
    if(@$_FILES['value']['name'] != ''){
      move_uploaded_file($_FILES['value']['tmp_name'], '../uploads/feed/' . $_REQUEST['feedImg']);
    }
    $status = "success";
  }

  $response = [
    "status" => $status
  ];
  echo json_encode($response);

}


if(@$_REQUEST['request'] == "getUserFeeds") //Request from react native app
{
  $status = "failed";
  $feeds = "";
  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $userId= $userData->id;
    $feeds = $obj_Feed->getUserfeeds($userId);
  }
  $response = [
    "status" => $status,
    "data" => $feeds
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "doLikePost") //Request from react native app
{
  $status = "failed";
  $getdata = json_decode(file_get_contents('php://input'), true);
    $insert = $obj_Feed->insertFeedLike($getdata);
    if($insert){
      $status = "success";
    }

  $response = [
    "status" => $status,
    "action" => $insert
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "deletePost") //Request from react native app
{
  $status = "failed";
  $getdata = json_decode(file_get_contents('php://input'), true);
  // $getdata = [];
  // $getdata['feedId'] = 148;
  // $getdata['user_id'] = 6;

  $delete = $obj_Feed->delete($getdata);
  if($delete){
    $status = "success";
  }
  $response = [
    "status" => $getdata,
  ];
  echo json_encode($response);
}

if(@$_REQUEST['request'] == "commentPost") //Request from react native app
{
  $status = "failed";
  $getdata = json_decode(file_get_contents('php://input'), true);
    $insert = $obj_Feed->insertFeedComment($getdata);
    if($insert){
      $status = "success";
    }

  $response = [
    "status" => $status
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "sharePost") //Request from react native app
{
  $status = "failed";
  $getdata = json_decode(file_get_contents('php://input'), true);
  $insert = $obj_Feed->insert("messages",$getdata);
  if($insert){
    $status = "success";
  }

  $response = [
    "status" => $getdata
  ];

  echo json_encode($response);

}

if(@$_REQUEST['request'] == "getAllFeeds") //Request from react native app
{
  //$getdata['userId'] = 1;
  $getdata = json_decode(file_get_contents('php://input'), true);
  $feeds = $obj_Feed->getAllfeeds($getdata['userId']);
  $response = [
    "item" => $feeds,
    "qwerty" => $getdata
  ];
   echo json_encode($response);

}

if(@$_REQUEST['request'] == "doUserFollowActions"){
  $status = "failed";
  $getdata = json_decode(file_get_contents('php://input'), true);
  $action= $obj_Users->doFollowUnfollow($getdata);
  if($action){
    $status = "success";
  }

  $response = [
    "status" =>  $status,
    "action" => $action
  ];
  echo json_encode($response);
}

if(@$_REQUEST['request'] == "getAllUserNotification") //Request from react native app
{

  $status = "failed";
  $getdata = json_decode(file_get_contents('php://input'), true);
  $notifications = $obj_Apps->loadNotifications($getdata['userId']);
   echo json_encode($notifications);

}

if(@$_REQUEST['request'] == "addReview"){
  $status = "failed";

  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $userId= $userData->id;
    if(@$_FILES['reviewImg']['name'] != ''){
        $_REQUEST['reviewImg'] = time()."review".$_FILES['reviewImg']['name'];
        if($_REQUEST['type'] == "TI"){
          $des_path = '../uploads/review/' . $_REQUEST['reviewImg'];
        }else{
          $des_path = '../uploads/course/review/' . $_REQUEST['reviewImg'];
        }
        move_uploaded_file($_FILES['reviewImg']['tmp_name'], $des_path);
    }else{
        $_REQUEST['reviewImg'] = '';
    }

    $insert = $obj_Apps->insertReview($_REQUEST,$userId);
    if($insert){
        $status = "success";
    }

  }

  $response = [
    "status" =>  $status
  ];
  echo json_encode($response);
}

if(@$_REQUEST['request'] == "updateUserAccount"){
  $status = "failed";

  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $userId= $userData->id;
    if(@$_FILES['profileImg']['name'] != ''){
        $_REQUEST['userProfileImage'] = time()."user".$_FILES['profileImg']['name'];
        $des_path = '../uploads/user/' . $_REQUEST['userProfileImage'];
        move_uploaded_file($_FILES['profileImg']['tmp_name'], $des_path);
    }

    $update = $obj_Users->updateUserAccount($_REQUEST,$userId);
    if($update){
        $status = "success";
    }

  }

  $response = [
    "status" =>  $status,
    "data" => $_REQUEST
  ];
  echo json_encode($response);
}


if(@$_REQUEST['request'] == "updateUserInfo"){
  $status = "failed";

  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $userId= $userData->id;
    if(@$_FILES['bgImg']['name'] != ''){
        $_REQUEST['userBgImage'] = time()."user".$_FILES['bgImg']['name'];
        $des_path = '../uploads/user/' . $_REQUEST['userBgImage'];
        move_uploaded_file($_FILES['bgImg']['tmp_name'], $des_path);
    }
    $update = $obj_Users->updateUserInfo($_REQUEST,$userId);
    if($update){
        $status = "success";
    }

  }

  $response = [
    "status" =>  $status,
    "data" => $_REQUEST
  ];
  echo json_encode($response);
}


if(@$_REQUEST['request'] == "updateUserPassword"){
  $status = "failed";
  $msg = "";
  if(isset($_SESSION['loggedIn'])){
    $userData = json_decode($_SESSION['userData']);
    $userId= $userData->id;
    $check_password= $obj_Users->checkUserPassword(md5($_REQUEST['old_password']),$userId);
    if($check_password>0){
      $update = $obj_Users->updateUserPassword(md5($_REQUEST['new_password']),$userId);
      if($update){
          $status = "success";
      }
    }else{
      $msg = "old password is wrong";
    }


  }

  $response = [
    "status" =>  $status,
    "data" => $_REQUEST
  ];
  echo json_encode($response);
}

if(@$_REQUEST['request'] == "getSeachResult") //Request from react native app
{
  $status = "failed";
  $getdata = json_decode(file_get_contents('php://input'), true);
  $search = $obj_Apps->searchTables($getdata['searchText']);
  if($search){
    $status = "success";
  }
  $response = [
    "searchResult" => $search
  ];
  echo json_encode($response);
}


if(@$_REQUEST['request'] == "addPushToken") //Request from react native app
{
  $status = "failed";
  $getdata = json_decode(file_get_contents('php://input'), true);
  $token = $obj_Apps->addExpoToken($getdata['token'],$getdata['userId']);
  if($token){
    $status = "success";
  }
  $response = [
    "result" => $getdata
  ];
  echo json_encode($response);
}

if(@$_REQUEST['request'] == "getUserPushNotificationToken") //Request from react native app
{
  $status = "failed";
  $getdata = json_decode(file_get_contents('php://input'), true);
  $token = $obj_Apps->getExpoPushNotificationToken($getdata['userId']);
  if($token){
    $status = "success";
  }
  $response = [
    "token" => $token
  ];
  echo json_encode($response);
}

if(@$_REQUEST['request'] == "getCourseCategories") //Request from react native app
{
  $status = "failed";
  $getCourseCategories = $obj_Course->getCourseCategoriesContainsVerifiedCourse();

  if($getCourseCategories){
    $status = "success";
    $rowCourseCategories = [];
    while($row_categories = mysqli_fetch_assoc($getCourseCategories)){
      $rowCourseCategories[] = $row_categories;
    }
  }
  $response = [
    "categories" => $rowCourseCategories
  ];
  echo json_encode($response);
}


if(@$_REQUEST['request'] == "getCourseListByCategories") //Request from react native app
{
  $status = "failed";
  $data = json_decode(file_get_contents('php://input'), true);
  //$data['categoryId'] =1;
  $getVerifiedCategoriesCourses = $obj_Course->getVerifiedCategoriesCourses($data['categoryId']);

  if($getVerifiedCategoriesCourses){
    $status = "success";
    $rowGetVerifiedCategoriesCourses = [];
    while($row_courses = mysqli_fetch_assoc($getVerifiedCategoriesCourses)){
      $rowGetVerifiedCategoriesCourses[] = $row_courses;
    }
  }

  echo json_encode($rowGetVerifiedCategoriesCourses);
}

if(@$_REQUEST['request'] == "setVisitedLessons") //Request from react native app
{
  $status = "failed";
  $data = json_decode(file_get_contents('php://input'), true);
  $update = $obj_Course->setVisitedLesson($data);
  if($update){
     $status = "success";
   }
 $response = [
   "status" => $status,
   "data" => $data
 ];
  echo json_encode($response);
}
if(@$_REQUEST['request'] == "updateLessonStatus") //Request from react native app
{
  $status = "failed";
  $data = json_decode(file_get_contents('php://input'), true);
  $update = $obj_Course->updateLessonStatus($data);
  if($update){
     $status = "success";
   }
 $response = [
   "status" => $status,
   "data" => $data
 ];
  echo json_encode($response);
}


if(@$_REQUEST['request'] == "userCourseLessonsStatus") //Request from react native app
{
  $status = "failed";
  $data = json_decode(file_get_contents('php://input'), true);
  //$data['courseId'] = 48;
  //$data['userId'] = 6;
  $select = $obj_Course->userCourseLessonsStatus($data);
  if($select){
     $status = "success";
   }
 $response = [
   "status" => $status,
   "data" => $select
 ];
  echo json_encode($response);
}

if(@$_REQUEST['request'] == "doFinishCourse") //Request from react native app
{
  $status = "failed";
  $data = json_decode(file_get_contents('php://input'), true);
  //$data['courseId'] = 48;
  //$data['userId'] = 6;
  $select = $obj_Course->finishedCourse($data);
  if($select){
     $status = "success";
   }
 $response = [
   "status" => $status,
   "data" => $select
 ];
  echo json_encode($response);
}

if(@$_REQUEST['request'] == "checkUserFinishCourseStatus") //Request from react native app
{
  $status = "failed";
  $data = json_decode(file_get_contents('php://input'), true);
  //$data['courseId'] = 48;
  //$data['userId'] = 6;
  $select = $obj_Course->checkUserToFinishCourse($data['courseId'],$data['userId']);
  if($select){
     $status = "success";
   }
 $response = [
   "status" => $status,
   "data" => $select
 ];
  echo json_encode($response);
}


?>
