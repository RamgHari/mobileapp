import React, { useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  Image,
  Text,
  ImageBackground
} from "react-native";
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import { Caption, Paragraph } from "react-native-paper";
import styles from './style.js';
import { useSelector} from 'react-redux';
const listTab=[{finish_status:'All',head:'Courses'},{finish_status:0,head:'InProgress'},{finish_status:1,head:'Finished'}]

const ProfileScreen =(props)=>{
const navigation = useNavigation();
const [finish_status,setStatus]=useState('All')
const [data,getCourse]=useState([])
const [datalist,setDataList]=useState([])
const [updated, setUpdated] = useState(false)

const users=useSelector(state => state.userReducer.user)

const retriveCourses=async()=>{
  const datas={userId:users.id}
 await  axios.post(`https://www.crick3tsocial.com/mobile/includes/ajaxFromApp.php?request=getUserProgressCourses`,datas)
  .then((res)  =>  res.data)
.then(
  (result) => {
    getCourse(result.data)
    setDataList(result.data)
    
  },
  (error) => {
    alert(error);
  } 
); 
}


React.useEffect(() => {
  retriveCourses();
}, [updated]);

const setStatusFilter=finish_status=>{
  if(finish_status!="All"){
    setDataList([...data.filter(e=>e.finish_status===finish_status)])
  }else{
    setDataList(data)
  }
  setStatus(finish_status)
}
const renderItem=({item,index})=>{
  const course=item
return(
   <TouchableOpacity key={course.id}  
   onPress={() => props.navigation.navigate("EnrollCourse",{item,users})}
   //onPress={()=>props.navigation.navigate('CoursePlay',{course,users})}
   >
    <View key={index} style={styles.itemContainer}>
    <View style={styles.itemLogo}>
    <Image style={styles.itemImage}
     source={{uri:`https://www.allskills.in/uploads/course/${course.image}`}}
     />
    </View>

    <View style={styles.itemBody}> 
      <Text style={styles.itemName}>{course.name}</Text>
    </View>
    </View> 
  </TouchableOpacity> 
  
)
}
return (
 
<SafeAreaView style={styles.containers}>  
         
        <View style={styles.titleBar}>
          <Ionicons name="ios-arrow-back" size={24} onPress={()=>props.navigation.goBack()} color="#52575D"></Ionicons>
          {/* <Ionicons name="md-more" size={24} color="#52575D"></Ionicons> */}
        </View>
            
        <View style={{ alignSelf: "center" }}>
          <View style={styles.profileImage}>
            <Image
              source={{ uri: users.userProfileImage}}
              style={styles.image}
              resizeMode="center"
            ></Image>
          </View>
          <View style={styles.dm}>
            <MaterialIcons name="chat" size={18} color="#DFD8C8" />
          </View>
        </View>
        <View style={styles.infoContainer}>
          {updated?<Text style={[styles.text, { fontWeight: "200", fontSize: 36 }]}>
            {users.first_name +' '+ users.last_name}
          </Text>:null}
          <Text style={styles.text}>{users.location}</Text>
          <Text style={styles.text}>{users.email}</Text>
          <Text style={styles.text}>{users.phone}</Text>
        </View>
        {/* <View style={styles.row}>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>{Math.floor(Math.random() * 100) + 2}</Paragraph>
                                <Caption style={styles.caption}>Following</Caption>
                            </View>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>{Math.floor(Math.random() * 100) + 5}</Paragraph>
                                <Caption style={styles.caption}>Followers</Caption>
                            </View>
                            </View> */}
{/* <View style={styles.listTab}>
{
  datalist.length>0?
  listTab.map(e=>
  <TouchableOpacity 
  key={e.head} 
  style={[styles.btnTab,finish_status===e.finish_status && styles.btnTabActivate]}
  onPress={()=>setStatusFilter(e.finish_status)}>
    <Text style={[styles.textTab,finish_status===e.finish_status && styles.txtTabActivate]}>{e.head}</Text>
  </TouchableOpacity>
  ):<Text style={styles.textTab}>No Courses Enrolled</Text>
}
</View>

<FlatList
data={datalist}
keyExtractor={(item,index) =>index.toString()}
renderItem={renderItem}
/> */}

</SafeAreaView>
)
}


export default ProfileScreen;