import { Video } from "expo-av";
import React, { useState } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  FlatList,
  ScrollView,
  Image,
  Button,
  BackHandler,
  Alert
} from "react-native";
import { useBackHandler } from "@react-native-community/hooks"
import { Ionicons, Feather } from "@expo/vector-icons";
import { WebView } from 'react-native-webview';
import { colors } from "../../../assets/color/style";
import { useSelector} from 'react-redux';
import axios from 'axios';
const CoursePlay = (props) => {
  const courseId=props.route.params.course;
  const users=useSelector(state => state.userReducer.user)
  const userId=users.id;
  const [lessons,setLessons]=React.useState(props.route.params.lessons);
  const [remainLesson,setReaminLesson]=React.useState(null)
  const [finishedCourese,setFinishedCourese]=React.useState(false)
  const [update,setUpdate]=React.useState(false)
  
  //console.log("lessonsdddd",lessons);
  const lessonId=lessons.id;
  let lessonData={...lessons}
  //console.log(lessonData)
  const {width, height} = Dimensions.get("window");
  const [error, setError] = React.useState(null);
  let datetime = new Date();

  const data={courseId:courseId,userId:userId,datetime:+datetime}
  const doFinishCourseData={courseid:courseId,userid:userId,datetime:+datetime}
  const updateLessondata={courseid:courseId,lessid:lessonId,user_id:userId,datetime:+datetime}
  const setVistedLessonData={lessonid:lessonId,userid:userId,datetime:+datetime}

  React.useEffect(() => {
    async function getData() {
    await axios.post(`https://www.allskills.in/includes/ajaxFromApp.php?request=checkUserFinishCourseStatus`,data)
    .then((res) =>res.data)
    .then(
      (result) => {  
       if(result[27]==1){
          setFinishedCourese(true) 
       }
      },
      (error) => {
      } 
    );
    }
    getData();
    getuserCourseLessonsStatus()
     
  

   axios.post(`https://www.allskills.in/includes/ajaxFromApp.php?request=setVisitedLessons`,setVistedLessonData)
  .then((res) =>res.data)
  .then(
    (result) => {
      setUpdate(true)
    },
    (error) => {
    
    } 
  ) 
  }, [])

  const getuserCourseLessonsStatus=()=>{
   
    axios.post(`https://www.allskills.in/includes/ajaxFromApp.php?request=userCourseLessonsStatus`,data)
    .then((res) =>res.data)
  .then(
    (result) => {
      //console.log("aa",result)
      setReaminLesson(result.data.lessonCount-result.data.statusCount)
    },
    (error) => {
      setError(err);
    } 
  );
  }
  const doFinishCourse=()=>{
    axios.post(`https://www.allskills.in/includes/ajaxFromApp.php?request=doFinishCourse`,doFinishCourseData)
    .then((res) =>res.data)
    .then(
    (result) => {
     setFinishedCourese(true)
     
    },
    (error) => {
    setError(err);
    } 
    );
}

  const updateLessonStatus=()=>{
      axios.post(`https://www.allskills.in/includes/ajaxFromApp.php?request=updateLessonStatus`,updateLessondata)
      .then((res) =>res.data)
      .then(
      (result) => {
        lessonData.status='1'
        setLessons(lessonData)
        getuserCourseLessonsStatus()

      },
      (error) => {
      setError(err);
      } 
      );
  }
  // Callback function for back action
    const backActionHandler = () => {
      props.navigation.navigate('CourseView',{ update: Math.floor(Math.random() * 100) + 2  })
      return true;
    };
  
  // hook which handles event listeners under the hood
    useBackHandler(backActionHandler)
    
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={{ paddingTop: 12, paddingLeft: 15 }}>
          <TouchableOpacity onPress={() => props.navigation.navigate('CourseView',{ update: Math.floor(Math.random() * 100) + 1  })}>
            <Ionicons name="md-arrow-back" size={24} color="#d8d9d8"></Ionicons>
          </TouchableOpacity>
        </View>
        <View style={{position: 'absolute', right: 0}}>
          
 {update?(lessons.status!=1  ?
  <TouchableOpacity  style={styles.appButtonContainer} onPress={updateLessonStatus}>
      <Text style={styles.appButtonText}>Finish Lesson</Text>
  </TouchableOpacity>:!finishedCourese?<Text style={styles.appButtonText}>You have Finished the Lesson</Text>:null):null}
    </View>
      </View>
      <ScrollView>
      <View>
      {update?(finishedCourese?<Text style={styles.appText}>You have finish this course</Text>:
               remainLesson>0 ? (  
                <Text style={styles.appText}>You have only {remainLesson} lesson to finish this course</Text>
      ) : (
        <TouchableOpacity  style={styles.appButtonContainer} onPress={doFinishCourse}>
      <Text style={styles.appButtonText}>Mark As Course Complete</Text>
      </TouchableOpacity>
      )) :null}
      <Text style={styles.lessonText}>{lessons.name}</Text>
        {lessons.video_file?
      <Video
       source={{uri:`https://www.allskills.in/uploads/course/lesson/video/${lessons.video_file}`}}
         rate={1.0}
         isMuted={false}
         resizeMode="cover"
         shouldPlay={true}
         isLooping={false}
         useNativeControls
         style={{width:width,
          height:height/3}} 
        />:<Text></Text>
         }
        {lessons.video_embed_url?
        <WebView
        style={{ alignSelf: "stretch", height: 248 }}
        source={{ uri: "https://www.youtube.com/embed/"+ video_embed_url}}
        />:<Text></Text>
}
{lessons.image?
         <Image
                    source={{uri:`https://www.allskills.in/uploads/course/lesson/${lessons.image}`}}
                    style={{
                        height:width,
                        width:400,
                        alignSelf:"center",
                    }}/>:<Text></Text>}
      </View>
      
            <View style={{ paddingHorizontal: 10}}>
              {/* <Text style={{ fontSize: 20 }}>{lessons.name}</Text> */}
                <WebView
              source={{
                html:
                  "<style>iframe{ width:100%;height:500px} p,li{font-size:60px;margin:20px}</style><div style='font-size:50px'>" +
                  lessons.content +
                  "<p style='text-align:center;font-size:50px'></p></div>",
              }}
              style={{ width: width,height:5000}}
                />
              </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  header: {
    backgroundColor: '#009387' ,
    flexDirection: "row",
    height: 50,
    //marginTop: 20,
    borderBottomColor: "black",
    borderBottomWidth: 1,
  },
  inputContainer: {
    position: "absolute",
    bottom: 5,
    paddingLeft: 32,
    flexDirection: "row",
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#009387",
    borderRadius: 10,
    paddingVertical: 12,
    paddingHorizontal: 12
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    
  },  
  lessonText: {
    fontSize: 18,
    fontFamily:'Bold',
    fontWeight: "bold",
    alignSelf: "center",
    
  },
  appText: {
    fontSize: 18,
    color: "#333",
    fontWeight: "bold",
    alignSelf: "center",
  }
});

export default CoursePlay;
