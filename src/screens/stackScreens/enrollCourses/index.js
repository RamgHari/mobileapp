import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { Feather, Entypo, Ionicons, FontAwesome5 } from "@expo/vector-icons";
import { colors } from "../../../assets/color/style";
import axios from 'axios';
import { useSelector} from 'react-redux';
import Chapters from "../../../components/chapters";
const EnrollScreen = (props) => {
  const course=props.route.params.item;
  const users=useSelector(state => state.userReducer.user)
  const [finishedCourese,setFinishedCourese]=React.useState(false)
  const [text,setText]=React.useState('')
  const [lists,setLists]=React.useState([])
  React.useEffect(() => {
      async function getData() {
        await axios.post(`https://www.allskills.in/includes/ajaxFromApp.php?request=getChaptersAndLessons`,{courseId:course.id,userId:users.id})
        .then((res)  =>  res.data)
      .then(
        (result) => {
           setLists(result.data)
           //console.log("Lists",lists)
        },
        (error) => {
          alert(error);
        } 
      );
      }
      getData();

      async function checkUserFinishCourseStatus() {
        
        axios.post(`https://www.allskills.in/includes/ajaxFromApp.php?request=checkUserFinishCourseStatus`,{courseId:course.id,userId:users.id})
        .then((res) =>res.data)
        .then(
          (result) => {  
            console.log("cc",result)
           if(result[27]==1){
              setFinishedCourese(true) 
              setText('Start Again')
              //console.log(finishedCourese)
           }else{
            setText('Start the Course')
           }
          },
          (error) => {
          } 
        );
        }
        checkUserFinishCourseStatus();
  }, []);


  return (
    <ScrollView style={{ backgroundColor: "#dbdbdb" }}>
      <View style={styles.container}>
        <ImageBackground
         source={{uri:`https://www.allskills.in/uploads/course/${course.image}`}}
          style={styles.image}
          imageStyle={{
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}
        >
         

          <TouchableOpacity
            style={{
              position: "absolute",
              left: 20,
              top: 40,
              backgroundColor: '#009387' ,
              padding: 10,
              borderRadius: 40,
            }}
            onPress={() => props.navigation.goBack()}
          >
            <Feather name="arrow-left" size={22} color="white" />
          </TouchableOpacity>

        </ImageBackground>
        {course.external_url==''?
        <TouchableOpacity
          style={styles.EnrollBtn}
          onPress={() => props.navigation.navigate("CourseView",{course,users})}
        >
        <Text style={styles.EnrollText}>{text}</Text>
        </TouchableOpacity>:
        <TouchableOpacity
        style={styles.EnrollBtn}
        onPress={() => props.navigation.navigate("CourseView",{course,users})}
      >
      <Text style={styles.EnrollText}>Go to Course</Text>
      </TouchableOpacity>}
      
      {course.external_url!= ''? <View style={styles.PriceBtn}>
          <Text style={styles.EnrollText}>External Course</Text>
        </View>:null}


        <View style={styles.textContainer}>
          <Text style={{ padding: 14, fontSize: 20, fontWeight: "bold" }}>
          {course.name}
          </Text>
          <Text
            style={{
              paddingHorizontal: 14,
              fontSize: 14,
              fontWeight: "normal",
              opacity: 0.3,
              justifyContent: "flex-start",
              textAlign: "justify",
              lineHeight: 26,
            }}
          >
           {course.description}
          </Text>
        </View>

        <View style={styles.textContainer}>
          <Text style={{ fontSize: 20, padding: 14 }}>
            Course by {course.userDetails.first_name +' '+ course.userDetails.last_name}
          </Text>
          <View style={styles.profileContainer}>
            <Image source={{uri:`https://www.allskills.in/uploads/course/${course.image}`}} style={styles.profileImage} />
            <View>
              {/* <View style={styles.profileText}>
                <FontAwesome5
                  name="user"
                  size={22}
                  color={colors.textGray}
                  style={{ paddingHorizontal: 10 }}
                />
                <Text
                  style={{
                    fontSize: 17,
                    color: colors.textGray,
                    paddingLeft: 3,
                    alignSelf: "center",
                  }}
                >
                  {parseInt(course.userDetails.id) + 500} Students
                </Text>
              </View>
              <View style={styles.profileText}>
                <FontAwesome5
                  name="react"
                  size={22}
                  color={colors.textGray}
                  style={{ paddingHorizontal: 10 }}
                />
                <Text
                  style={{
                    fontSize: 17,
                    color: colors.textGray,
                    paddingLeft: 3,
                    alignSelf: "center",
                  }}
                >
                  {parseInt(course.userDetails.id) + 100} Courses
                </Text>
              </View> */}
              {/* <View style={styles.profileText}>
                <FontAwesome5
                  name="star"
                  size={22}
                  color={colors.textGray}
                  style={{ paddingHorizontal: 10 }}
                />
                <Text
                  style={{
                    fontSize: 17,
                    color: colors.textGray,
                    paddingLeft: 3,
                    alignSelf: "center",
                  }}
                >
                  4.8 User Rating
                </Text>
              </View> */}
            </View>
          </View>

         <TouchableOpacity
          onPress={() =>
            course.userDetails.id != users.id
              ? props.navigation.navigate("UserProfile", course.userDetails)
              : props.navigation.navigate("Profile", course.userDetails)
          }
        >
          <Text
            style={{
              fontSize: 16,
              color: colors.textGray,
              textAlign: "center",
              paddingTop: 12,
            }}
          >
            View Profile
          </Text>
          </TouchableOpacity>
        </View>
        {course.external_url==''?
        <View style={styles.textContainer}>
                          <Text style={{ fontSize: 20, padding: 14 }}>Course Outline</Text>
                   {
                    lists.length>0?lists.map((item,index)=>{ 
                        return(<Chapters  index={index}  key={item.id}   lessons={item}/>)
                    })
                    :<Text></Text>}
         </View>:null}

       </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  image: {
    height: 280,
    justifyContent: "flex-end",
  },

  Tagline: {
    color: colors.headerColor,
    fontSize: 16,
    fontWeight: "bold",
    paddingHorizontal: 14,
    marginVertical: 6,
  },
  Placename: {
    color: colors.headerColor,
    fontSize: 24,
    fontWeight: "bold",
    paddingHorizontal: 14,
    marginBottom: 30,
  },
  EnrollText: {
    color: "white",
    fontSize: 14,
  },
  PriceBtn: {
    position: "absolute",
    left: 12,
    top: 250,
    backgroundColor: '#009387' ,
    padding: 16,
    borderRadius: 30,
    elevation: 5,
  },
  EnrollBtn: {
    position: "absolute",
    right: 12,
    top: 250,
    backgroundColor: '#009387' ,
    padding: 16,
    borderRadius: 30,
    elevation: 5,
  },
  darkOverlay: {
    width: 150,
    height: 150,
    position: "absolute",
    backgroundColor: "#000",
    opacity: 0.3,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    borderRadius: 10,
    marginHorizontal: 10,
  },
  textContainer: {
    backgroundColor: "white",
    margin: 5,
    paddingVertical: 10,
  },
  descsContainer: {
    marginTop: 16,
    marginHorizontal: 16,
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  descContainer: {
    alignItems: "center",
    width: 95,
    marginVertical: 12,
  },
  description: {
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
  descName: {
    color: "white",
    fontSize: 12,
    fontWeight: "600",
    marginTop: 6,
    textAlign: "center",
  },
  sectionContainer: {
    paddingVertical: 24,
    paddingHorizontal: 32,
    margin: 5,
    marginTop: 10,
    backgroundColor: "#292929",
  },
  gsTitle: {
    fontWeight: "700",
    color: "white",
    fontSize: 24,
    textAlign: "center",
  },
 
  profileContainer: {
    flexDirection: "row",
  },
  profileImage: {
    width: 100,
    height: 100,
    marginHorizontal: 10,
  },
  profileText: {
    flexDirection: "row",
    marginVertical: 5,
  },
  bottomButtom: {
    flexDirection: "row",
  },
});

export default EnrollScreen;
