import React, { useState } from "react";
import {
  Text,
  TextInput,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import { Feather, Ionicons } from "@expo/vector-icons";
import axios from 'axios';
import { FlatList } from "react-native-gesture-handler";
import { colors } from "../../../assets/color/style";
import { useSelector} from 'react-redux';
const CategoryScreen = ({ navigation }) => {
  const users=useSelector(state => state.userReducer.user)
  const [category,setcategory]=React.useState([])
  const [query, setQuery] = useState('');
  const [fullData, setFullData] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(false);
  const [error, setError] = React.useState(null);
  
  React.useEffect(() => {
    setIsLoading(true);
    getCategory()
  }, []);
 
  const getCategory=()=>{
    axios.get(`https://www.crick3tsocial.com/mobile/includes/ajaxFromApp.php?request=getCourseCategories`)
    .then((res) =>res.data)
  .then(
    (result) => {
      setIsLoading(false);
      setcategory(result.categories)
      setFullData(result.categories);
    },
    (error) => {
      setIsLoading(false);
      setError(error);
    } 
  );
  }

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color="#5500dc" />
      </View>
    );
  }

  if (error) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text style={{ fontSize: 18}}>
          Error fetching data... Check your network connection!
        </Text>
      </View>
    );
  }
  function renderHeader() {
    return (
      <View style={{ paddingVertical: 20, backgroundColor: '#009387',flexDirection:'row',alignItems:'center'}}>
        <TouchableOpacity style={{ marginLeft:40}}
            onPress={() => navigation.goBack()}
          >
            <Feather name="arrow-left" size={22} color="white" />
          </TouchableOpacity>
      <TextInput
        style={styles.searchBox}
        autoCorrect={false}
        autoCapitalize="none"
        value={query}
        onChangeText={queryText => handleSearch(queryText)}
        placeholder="Search"
      ></TextInput>
      {/* <Feather
        name="search"
        size={22}
        color="#666"
        style={{ position: "absolute", top: 50, right: 60, opacity: 0.6 }}
      /> */}
    </View>
    );
  }

  const handleSearch = text => {
    const formattedQuery = text.toLowerCase();
    const filteredData = fullData.filter((category) => {
      return category.name.toLowerCase().indexOf(formattedQuery.toLowerCase())>=0
    });
    setcategory(filteredData);
    setQuery(text);
  };
  
  return (
    <View style={styles.container}>
      <FlatList
        data={category}
        keyExtractor={(item,index) =>index.toString()}
        ListHeaderComponent={renderHeader}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity  onPress={() => navigation.navigate("Courses",{item})} >
              <View style={styles.cateContainer}>
                <View styles={{ width: "20%" }}>
                  <Ionicons
                    name='ios-desktop'
                    size={30}
                    color={item.color}
                    style={{ marginLeft: 35 }}
                  />
                </View>
                <View
                  style={{ alignSelf: "center", paddingLeft: 40, width: "80%" }}
                >
                  <Text style={{ fontSize: 16, color: "grey" }}>
                  {item.name}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  searchContainer: {
    paddingTop: 100,
    paddingLeft: 16,
  },
  setTitle: {
    fontSize: 38,
    fontWeight: "bold",
    color: colors.defaultRed,
  },
  setTitleText: {
    fontSize: 16,
    fontWeight: "normal",
    color: colors.defaultRed,
  },

  searchBox: {
    marginTop: 16,
    backgroundColor: "#fff",
    paddingLeft: 24,
    padding: 12,
    borderTopRightRadius: 40,
    borderBottomRightRadius: 40,
    width: "70%",
    marginLeft:40
  },
  cateContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 10,
    backgroundColor: "#fff",
  },
});

export default CategoryScreen;