import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  SafeAreaView
} from "react-native";
import { Avatar } from "react-native-paper";
import like from "../../../assets/images/like.png";
import liked from "../../../assets/images/heart.png";
import send from "../../../assets/images/send.png";
import comment from "../../../assets/images/comment.png";
import { ScrollView } from 'react-native-gesture-handler';
import { WebView } from "react-native-webview";
import Timeago from "../../../components/time";
import Comments from "../../../components/comments";
import { MaterialCommunityIcons, FontAwesome5 } from "@expo/vector-icons";
import { Container } from 'native-base';
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from "react-native-popup-menu";
import { useSelector,useDispatch} from 'react-redux';
import {onDeleteFeed,onLikeFeed,onSubmitComment,onSharePost} from '../../../redux'

const SinglePost = (props) => {
  const dispatch = useDispatch()
  const users=useSelector(state => state.userReducer.user)
  const item=useSelector(state => state.userReducer.feed[props.route.params])

  const openModal=useSelector(state => state.userReducer.openModal)
  const postId=useSelector(state => state.userReducer.cmtPostId)
  const error =useSelector(state => state.userReducer.appError)

  //uj@jd0vnEinds&0_kfsf2]vk03@kfhx

  React.useEffect(() => {
   
  }, [useSelector(state => state.userReducer.feed)]);

  const handleComment = (id) => {
    dispatch({type:'OPEN_MODAL',key:openModal,postId:id})
    
  };

  const closeModal = () => {
    dispatch({type:'OPEN_MODAL',key:openModal,postId:''})
  };
  const DeleteFeed=async(feedId,user_id)=>{
    await dispatch(onDeleteFeed(feedId, user_id))
    props.navigation.navigate('Home')
  }

  const shareFeed=async(feedId,feed_user_id,user_id)=>{
    await dispatch(onSharePost(feedId,feed_user_id,user_id))
    props.navigation.navigate('Home')
  }
  if (error) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text style={{ fontSize: 18 }}>
          Error fetching data... Check your network connection!
        </Text>
      </View>
    );
  }

  const RenderProfile = ({ user }) => {
    const feedId = user.id;
    const user_id = user.user.id;
    return (
      <View style={styles.feedItemHeader}>
        <TouchableOpacity
          onPress={() =>
            user.user.id != users.id
              ? props.navigation.navigate("UserProfile", user.user)
              : props.navigation.navigate("Profile", user.user)
          }
        >
          <View style={styles.imgInfo}>
            <Avatar.Image source={{ uri: user.user.image }} size={40} />
            <View style={styles.userInfo}>
              <Text style={styles.name}> {user.user.first_name}</Text>
              <TouchableOpacity
                onPress={() => props.navigation.navigate("SinglePost")}
              >
                <Timeago time={user.time} />
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
        {user.user.id == users.id ? (
          <Menu style={styles.popupMenu}>
            <MenuTrigger>
              <MaterialCommunityIcons name="dots-vertical" size={26} />
            </MenuTrigger>
            <MenuOptions>
              <MenuOption
                onSelect={() => DeleteFeed(feedId, user_id)}
                text="Delete"
              />
              {/* <MenuOption onSelect={() => alert(`Delete`)} >
          <Text style={{color: 'red'}}>Delete</Text>
        </MenuOption>
        <MenuOption onSelect={() => alert(`Not called`)} disabled={true} text='Disabled' /> */}
            </MenuOptions>
          </Menu>
        ) : null}
      </View>
    );
  };
  return (
      <Container>
            <SafeAreaView style={styles.container}>
                <ScrollView>
                         <View style={styles.titleBar}>
          <Ionicons name="ios-arrow-back" size={24} onPress={() =>
          props.navigation. navigate('Home')
        } color="#52575D"></Ionicons>
        </View>
          <View style={styles.feedItem}>
            {item.user.id != users.id ? (
              <RenderProfile user={item} />
            ) : (
              <RenderProfile user={item} />
            )}

            <View>
              {item.type == "picture" ? (
                <View>
                  <Image
                    style={styles.feedImage}
                    source={{ uri: `${item.post_image}` }}
                  />
                  <Text style={styles.feedText}>{item.message}</Text>
                </View>
              ) : item.type == "video" ? (
                <Video
                  source={{
                    uri: `https://www.allskills.in/uploads/course/lesson/video/${item.video_file}`,
                  }}
                  rate={1.0}
                  style={styles.feedImage}
                  isMuted={false}
                  resizeMode="cover"
                  shouldPlay={true}
                  isLooping={false}
                  useNativeControls
                />
              ) : item.type == "youtube" ? (
                <WebView
                  style={styles.feedImage}
                  source={{
                    uri: "https://www.youtube.com/embed/" + item.value,
                  }}
                />
              ) : item.type == "shared" ? (
                item.sharedFeedDetails.details.type == "picture" ? (
                  <View>
                    <Image
                      style={styles.feedImage}
                      source={{ uri: `${item.sharedFeedDetails.post_image}` }}
                    />
                    <Text style={styles.feedText}>
                      {item.sharedFeedDetails.details.message}
                    </Text>
                  </View>
                ) : item.sharedFeedDetails.details.type == "youtube" ? (
                  <WebView
                    style={styles.feedImage}
                    source={{
                      uri:
                        "https://www.youtube.com/embed/" +
                        item.sharedFeedDetails.details.value,
                    }}
                  />
                ) : (
                  <Text style={styles.feedText}>
                    {item.sharedFeedDetails.details.message}
                  </Text>
                )
              ) : (
                <Text style={styles.feedText}>{item.message}</Text>
              )}
             
            </View>

            <View style={styles.feedItemFooter}>
              <View style={styles.actions}>
                <TouchableOpacity
                  style={styles.action}
                  onPress={() => dispatch(onLikeFeed(item.id,users.id))}
                >
                  {item.userLikedStatus ? (
                    <Image source={liked} />
                  ) : (
                    <Image source={like} />
                  )}
                  {/* <Text>{item.from_id}</Text> */}
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.action}
                  onPress={() => handleComment(item.id)}
                >
                  <Image source={comment} />
                </TouchableOpacity>
                
                {users.id == item.user.id ? null : (
                  <TouchableOpacity
                    style={styles.action}
                    onPress={() => shareFeed(item.id, item.user.id,users.id)}
                  >
                    <Image source={send} />
                  </TouchableOpacity>
                )}
              </View>
            </View>
            <TouchableOpacity
              onPress={() =>
                item.user.id != users.id
                  ? props.navigation.navigate("UserProfile", item.user)
                  : props.navigation.navigate("Profile", item.user)
              }
            >
              { item.feedComments!='' ?
        
        item.feedComments.map((item,index)=>{
                        return(
                        <View style={styles.commentcontainer} key={index}>
                        
                            <View style={styles.imgInfo}>
                                     
                            <Avatar.Image 
                            source={{uri:item.userDetails.image}}
                            size={40}/>
                           
                           
                            <View style={styles.userInfo}>
                                    <Text > {item.userDetails.first_name}</Text>
                          <Text style={styles.comment}> {item.comment}</Text>
                                    
                            </View>
                            </View>
                            
                           </View>
                           )
               })
           
                        :null}
            </TouchableOpacity>

           
          </View>

      {openModal ? (
        <Comments
          closeModal={closeModal}
          //handlesubmitComment={(cmt) => submitComment(cmt)}
          handlesubmitComment={(cmt) => dispatch(onSubmitComment(postId,users.id,cmt))}
        />
      ) : null}
    </ScrollView>
    </SafeAreaView>
    </Container>
  );
};
export default SinglePost;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  commentcontainer: {
    margin: 10,
    paddingTop: 10,
    paddingRight: 10,
    paddingLeft: 10,
    //   borderColor: '#009387',
    //   borderWidth: 1,
    //  // borderStyle:"dashed",
    borderRadius: 10,
    backgroundColor: "#fff",
  },
  input: {
    flex: 1,
  },
  feedItem: {
    marginTop: 20,
  },
  titleBar: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 24,
    marginHorizontal: 16,
  },
  imgInfo: {
    flexDirection: "row",
  },
  userInfo: {
    marginLeft: 10,
    flexDirection: "column",
    justifyContent: "center",
  },
  feedItemHeader: {
    paddingHorizontal: 15,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  name: {
    fontSize: 14,
    color: "#000",
  },
  comment: {
    fontSize: 14,
    color: "#000",
    fontFamily: "Regular",
  },
  place: {
    fontSize: 12,
    color: "#666",
  },
  feedImage: {
    width: "100%",
    height: 250,
    marginVertical: 15,
  },
  feedText: {
    width: "100%",
    marginVertical: 15,
    marginLeft: 8,
    fontFamily: "Medium",
    alignItems: "center",
    justifyContent: "center",
  },
  feedItemFooter: {
    paddingHorizontal: 15,
    marginBottom: 15,
    borderBottomColor: "#333",
  },
  actions: {
    flexDirection: "row",
  },
  action: {
    marginRight: 15,
  },
  likes: {
    marginTop: 15,
    fontWeight: "bold",
    color: "#000",
  },
  description: {
    lineHeight: 18,
    color: "#000",
  },
  hashtags: {
    color: "#7159c1",
  },
  last: {
    marginBottom: 15,
  },
});
