import React, { useEffect } from 'react'
import {View,Text} from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { colors } from "../../assets/color/style";
import CourseLessons from '../courseLessons';

const CourseChapters=(props)=>{
  
        const {course,name,lessonDetails} = props.lessons
        const users=props.users
       
        const navigation = useNavigation();
        return(
            <View style={{ flexDirection: "row",
            paddingHorizontal: 8,
            paddingBottom: 5,
            paddingVertical: 5}}>
            
            <View style={{ paddingLeft: 10 }}>
              <Text style={{ fontSize: 17, color: colors.textGray }}>{name}</Text>
              {lessonDetails.map((item,index)=>{
                //   console.log(course)
                 return(<CourseLessons onPress={()=>navigation.navigate('CoursePlay',{lessons:item,course:course,users:users})}  key={index}  index={index} count={index+1} lessonDetails={item}/>)
        })
    }
            </View>
</View>
        )
    }



export default CourseChapters