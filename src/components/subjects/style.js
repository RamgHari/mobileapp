import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container:{
            flexDirection:"row",
            backgroundColor:"#009387",
            padding:20,
            marginHorizontal:20,
            borderRadius:20,
            alignItems:"center",
            marginTop:10
    },
    head:{
        color:"#fff",
        fontFamily:"Bold",
        fontSize:13,
        paddingHorizontal:20,
        width:170
    }

});
