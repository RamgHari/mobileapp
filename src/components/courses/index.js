import React from 'react'
import {Text,TouchableOpacity, View,Image,StyleSheet} from 'react-native'
import ProgressCircle from 'react-native-progress-circle'
import styles from './style.js';
const  CourseList=(props)=>{

        const {image,name,id} = props.courses
        const onPress=props.onPress
        return(
            <TouchableOpacity
              onPress={onPress}
                style={styles.touch}>
                    <Image
                        source={{uri:`https://www.allskills.in/uploads/course/${image}`}}
                        style={{width:40,height:40}}
                    />
                    <View>
                         <Text style={styles.courseName}>{name}</Text>
                    </View>
                    <Text style={styles.space}></Text>
                    <ProgressCircle
                        percent={30}
                        radius={17}
                        borderWidth={1.5}
                        color="f580084"
                        shadowColor="#FFF"
                        bgColor="#FFF">
                        <Image
                            source={require('../../assets/images/pl.png')}
                        />
                    </ProgressCircle>

            </TouchableOpacity>
        )
    
}

export default CourseList;