import React, { useEffect } from 'react'
import {View,Text,Image,TouchableOpacity,StyleSheet} from 'react-native'
import ProgressCircle from 'react-native-progress-circle'
import styles from './style.js';
import { colors } from "../../assets/color/style"
 const Lessons=(props)=>{
    const {name,id}=props.lessonDetails
    const index=props.index
    return(
        <View>
        <Text style={{ fontSize: 13, color: colors.textGray }}>
       {index+1}.{name}
        </Text>
        </View>
     )
}


export default Lessons