import axios from "axios";
let datetime = new Date();
let appUrl='https://www.crick3tsocial.com/mobile/includes/ajaxFromApp.php?request=';
//action
export const onUserLogin=(username,password)=>{
    const data={signinUsername:username,signinPassword:password}
    return async(dispatch)=>{
        try {
            const response=await axios.post(`${appUrl}loginUser`,data)
            console.log("response",response)
            if(response.data.status=='success'){ 
                dispatch({type:'DO_LOGIN',payload:response.data.user,token:'logged'})
            }else{
                dispatch({type:'DO_LOGIN',token:Math.floor(Math.random() * 100) + 1})
                }
           
        } catch (error) {
            alert("Check Internet")
            dispatch({type:'ON_ERROR',payload:error})  
        }
        
    }
}


export const onUserSignUp=(email,firstname,lastname,password)=>{
    const data={signupEmail:email,signupFirstName:firstname,signupLastName:lastname,signupPassword:password,datetime:+datetime}
    return async(dispatch)=>{
        if(data.signupEmail!=''){
        try {
            const response=await axios.post(`${appUrl}registerAppUser`,data)
            //console.log(response,"response")
            if(response.data.status=='success'){
                dispatch({type:'DO_LOGIN',payload:response.data.user,token:'logged'})
                
            }else{
                dispatch({type:'DO_SIGNUP_ERROR',payload:response.data.message})
            }
           
        } catch (error) {
            alert("Check Internet")
            //dispatch({type:'ON_ERROR',payload:error})  
        }
    }else{
        dispatch({type:'SIGNUP_ERROR',error:"Please provide all the details"}) 
    }
        
    }
}

export const setLoading=()=>{
    return async(dispatch)=>{
        try {
            dispatch({type:'DO_LOADING'})
        } catch (error) {
            dispatch({type:'DO_LOADING'}) 
        }
        
    }
}
export const onUserSocialLogin=(response)=>{
    return async(dispatch)=>{
        try {
            dispatch({type:'DO_LOGIN',payload:response.user,token:'logged'})
        } catch (error) {
            dispatch({type:'DO_LOGIN',token:Math.floor(Math.random() * 100) + 1}) 
        }
        
    }
}


export const onUserLogout=()=>{
    return (dispatch)=>{
          dispatch({ type: 'LOGOUT' });
        
    }
}

//johnrameshaccet
export const onFetchFeed=(userId)=>{
    return async(dispatch)=>{
        try {
            const response=await axios.post(`${appUrl}getAllFeeds`,{userId: userId})
            console.log("allfeeds",response); 
            dispatch({type:'FETCH_FEED',payload:response.data.item})
        } catch (error) {
            dispatch({type:'ON_ERROR',payload:error})  
        }
        
    }
}

export const onLikeFeed=(feedId,user_id)=>{
    let data = { user_id: user_id, feedId: feedId, datetime: +datetime }
    return async(dispatch)=>{
        try {
            const response=await axios.post(`${appUrl}doLikePost`,data)
            dispatch({type:'LIKE_FEED',key:feedId})
        } catch (error) {
            dispatch({type:'ON_ERROR',payload:error})  
        }
        
    }
}

export const onSubmitComment=(PostId,user_id,cmt)=>{
    let data = {feedId: PostId,user_id: user_id,commentContent: cmt,datetime: +datetime,
      };
    //console.log("data",data)
    return async(dispatch)=>{
        try {
            const response=await axios.post(`${appUrl}commentPost`,data)
            console.log("aaaaa",response.data.commentsDetails)
            dispatch({type:'COMMENT_FEED',key:PostId,payload:response.data.commentsDetails})
        } catch (error) {
            dispatch({type:'ON_ERROR',payload:error})  
        }
        
    }
}

export const onSubmitPost=(image,content,user,Imageupdated)=>{
    const formData = new FormData();
    let datetime = new Date();
    
    const updated=Imageupdated;
    const uri = image;
    const urlData='';
    return async(dispatch)=>{
        try 
        {
            dispatch({type:'SET_IMAGE_UPLOAD'}) 
            dispatch({type:'SET_UPLOADING'})
            if(uri){
              if(uri &&  updated ){
                
                const uriParts = uri.split('.');
                const fileType = uriParts[uriParts.length - 1];
                formData.append('value', {
                  uri,
                  name: `photo.${fileType}`,
                  type: `image/${fileType}`,
                });
              }else{
                formData.append('value', content);
              }
              formData.append('type', "picture");
            }else{
              formData.append('type', "");
              formData.append('value', "");
            }
            
            formData.append('feedContent', content);
            formData.append('userId', user);
            formData.append('urlData', urlData);
            formData.append('tag', "");
            
            formData.append('datetime', +datetime);
            console.log("ffff",content,user,+datetime)
            if( content || image != '' ){
                const response=await axios
                .post(`${appUrl}postFeedFromApp`, formData, {
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'multipart/form-data',
                    },
                }).then((response) => {
                    console.log(response,"responsesssss")
                  dispatch({type:'SET_UPLOADING'})
                  dispatch({type:'SET_CONTENT',value:''})
                  dispatch({type:'SET_IMAGE',value:null})
                  dispatch({type:'SET_IMAGE_UPLOAD'})
                  dispatch({type:'SET_FEED',payload:response.data.feedDetails})
                  })
                }else{
                  dispatch({type:'SET_UPLOADING'})
                  alert("Check Input");
                }
            
        } catch (error) {
            dispatch({type:'ON_ERROR',payload:error})  
        }
}
    
}

export const onSharePost=(feedId,id,user)=>{
    let data = {user_id: user,message: "",type: "shared",value: feedId,from_id: id,urlData: "",time: +datetime,
      };
    return async(dispatch)=>{
        try {
            const response=await axios.post(`${appUrl}sharePost`,data)
            console.log(response,"response")
            dispatch({type:'SET_FEED',payload:response.data.feedDetails})
        } catch (error) {
            dispatch({type:'ON_ERROR',payload:error})  
        }
        
    }
}

export const onFetchSingleFeed=(feed)=>{
   
    return async(dispatch)=>{
        try {
            dispatch({type:'SET_SINGLE_POST',value:feed})
        } catch (error) {
            dispatch({type:'ON_ERROR',payload:error})  
        }
        
    }
}
export const onDeleteFeed=(feedId,user_id)=>{
    let data = { feedId: feedId, user_id: user_id, datetime: +datetime };
    return async(dispatch)=>{
        try {
            const response=await axios.post(`${appUrl}deletePost`,data)
            dispatch({type:'DELETE_FEED',key:feedId})
        } catch (error) {
            dispatch({type:'ON_ERROR',payload:error})  
        }
        
    }
}
