import { StyleSheet } from 'react-native';

export default StyleSheet.create({
welcome:{
    paddingHorizontal:20,
    fontSize:35,
    paddingTop:40,
    fontFamily:"Bold",
    color:"#FFF"
},
container:{
    flexDirection:"row",
    alignItems:"center",
    backgroundColor:"#FFF",
    padding:10,
    borderRadius:12,
    marginHorizontal:20,
    marginTop:20
},
txtInput:{
    fontFamily:"Bold",
    fontSize:12,
    width:280,
    paddingHorizontal:12
},
img:{
    height:14,
    width:14
},
head:{
    color:"#333",
    fontFamily:"Bold",
    fontSize:20,
    paddingHorizontal:20,
    marginTop:20,
    marginBottom:10
}
});