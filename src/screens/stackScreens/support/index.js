import * as React from 'react';
import { Button, View, Text, StyleSheet } from 'react-native';
import styles from './style.js';


const  SupportScreen=() =>{
  return (
    <View style={styles.container}>
      <Text>SupportScreen</Text>
      <Button
        title="Click Here"
        onPress={() => alert('Clicked')}
      />
    </View>
  );
}

export default SupportScreen;
